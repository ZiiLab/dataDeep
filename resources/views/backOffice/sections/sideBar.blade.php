<div id="page-sidebar">
    <div class="scroll-sidebar">
        <ul id="sidebar-menu">
            <li class="header"><span>Dashboard</span></li>
            <li>
                <a href="#" title="Dashboard">
                    <i class="glyph-icon icon-line-chart"></i>
                    <span>Statistics</span>
                </a>
            </li>
            <li class="header"><span>Blog</span></li>
            <li>
                <a href="#" title="Exploitation">
                    <i class="glyph-icon  icon-bookmark-o"></i>
                    <span>Categories</span>
                </a>
                <div class="sidebar-submenu">
                    <ul>
                        <li><a href="#" title="Channels"><span>List</span></a></li>
                        <li><a href="#" title="Subscriptions"><span>Add New</span></a></li>
                    </ul>
                </div>
                <!-- .sidebar-submenu -->
            </li>
            <li>
                <a href="#" title="Incomes">
                    <i class="glyph-icon icon-file-text-o"></i>
                    <span>Articles</span>
                </a>
                <div class="sidebar-submenu">
                    <ul>
                        <li><a href="#" title="Content"><span>List</span></a></li>
                        <li><a href="#" title="Ads"><span>Add New</span></a></li>
                    </ul>
                </div>
                <!-- .sidebar-submenu -->
            </li>

            <li>
                <a href="#" title="Incomes">
                    <i class="glyph-icon icon-comment-o"></i>
                    <span>Comments</span>
                </a>
                <!-- .sidebar-submenu -->
            </li>

            <li class="header"><span>Settings</span></li>
            <li>
                <a href="#" title="Paramètres">
                    <i class="glyph-icon icon-linecons-cog"></i>
                    <span>General Settings</span>
                </a>
                <div class="sidebar-submenu">
                    <ul>
                        <li><a href="#" title="Lots"><span>Users</span></a></li>
                    </ul>
                </div>
                <!-- .sidebar-submenu -->
            </li>
        </ul>
        <!-- #sidebar-menu -->
    </div>
</div>