
<!-- WIDGETS -->
<script type="text/javascript" src="{{ asset ('backOffice/plugins') }}/bootstrap/bootstrap.js"></script>

<!-- Bootstrap Dropdown -->
<!-- <script type="text/javascript" src="../../assets/widgets/dropdown/dropdown.js"></script> -->

<!-- Bootstrap Tooltip -->
<script type="text/javascript" src="{{asset('backOffice/plugins')}}/tooltip/tooltips.js"></script>

<!-- Bootstrap Popover -->
<!-- <script type="text/javascript" src="../../assets/widgets/popover/popover.js"></script> -->

<!-- Bootstrap Progress Bar -->
<script type="text/javascript" src="{{ asset ('backOffice/plugins') }}/progressbar/progressbar.js"></script>

<!-- Bootstrap Buttons -->
<!-- <script type="text/javascript" src="../../assets/widgets/button/button.js"></script> -->

<!-- Bootstrap Collapse -->
<!-- <script type="text/javascript" src="../../assets/widgets/collapse/collapse.js"></script> -->

<!-- Superclick -->
<script type="text/javascript" src="{{ asset ('backOffice/plugins') }}/superclick/superclick.js"></script>

<!-- Input switch alternate -->
<script type="text/javascript" src="{{ asset ('backOffice/plugins') }}/inputswitch/inputswitch.js"></script>
<script type="text/javascript" src="{{ asset ('backOffice/plugins') }}/inputswitch/inputswitch-alt.js"></script>

<!-- Slim scroll -->
<script type="text/javascript" src="{{ asset ('backOffice/plugins') }}/slimscroll/slimscroll.js"></script>

<!-- Slidebars -->
<script type="text/javascript" src="{{ asset ('backOffice/plugins') }}/slidebars/slidebars.js"></script>
<script type="text/javascript" src="{{ asset ('backOffice/plugins') }}/slidebars/slidebars-demo.js"></script>

<!-- PieGage -->
<script type="text/javascript" src="{{ asset ('backOffice/plugins') }}/piegage/piegage.js"></script>
<script type="text/javascript" src="{{ asset ('backOffice/plugins') }}/piegage/piegage-demo.js"></script>

<!-- Screenfull -->
<script type="text/javascript" src="{{ asset ('backOffice/plugins') }}/screenfull/screenfull.js"></script>

<!-- Content box -->
<script type="text/javascript" src="{{ asset ('backOffice/plugins') }}/contentbox/contentbox.js"></script>

<!-- Overlay -->
<script type="text/javascript" src="{{ asset ('backOffice/plugins') }}/overlay/overlay.js"></script>

<!-- Widgets init for demo -->
<script type="text/javascript" src="{{ asset ('backOffice/js') }}/widgets-init.js"></script>

<!-- Theme layout -->
<script type="text/javascript" src="{{ asset ('backOffice/js') }}/layout.js"></script>

<!-- Theme switcher -->
<script type="text/javascript" src="{{ asset ('backOffice/plugins') }}/themeswitcher/themeswitcher.js"></script>

<!-- sparklines -->
<script type="text/javascript" src="{{ asset ('backOffice/plugins') }}/sparklines/sparklines.js"></script>
<script type="text/javascript" src="{{ asset ('backOffice/plugins') }}/sparklines/sparklines-demo.js"></script>

<!-- Bootstrap Timepicker -->
<link rel="stylesheet" type="text/css" href="{{ asset ('backOffice/plugins') }}/timepicker/timepicker.css">
<script type="text/javascript" src="{{ asset ('backOffice/plugins') }}/timepicker/timepicker.js"></script>

<!-- Bootstrap Toggle -->
<link rel="stylesheet" type="text/css" href="{{ asset ('backOffice/plugins') }}/bootstrap-toggle-2/bootstrap-toggle.css">
<script type="text/javascript" src="{{ asset ('backOffice/plugins') }}/bootstrap-toggle-2/bootstrap-toggle.js"></script>

{{--<!-- DataTables -->--}}
{{--<link rel="stylesheet" type="text/css" href="{{ asset ('backOffice/plugins') }}/datatable_frontoffice/datatable.css">--}}
{{--<script type="text/javascript" src="{{ asset ('backOffice/plugins') }}/datatable_frontoffice/datatable.js"></script>--}}

{{--<!-- Float char -->--}}
<script type="text/javascript" src="{{ asset ('backOffice/plugins') }}/chart/flot.js"></script>

<!-- Moment -->
<script type="text/javascript" src="{{ asset ('backOffice/plugins') }}/moment/moment.js"></script>
<!-- DateRangePicker -->
<script type="text/javascript" src="{{ asset ('backOffice/plugins') }}/daterangepicker/daterangepicker.js"></script>
<!-- DatePicker -->
<script type="text/javascript" src="{{ asset ('backOffice/plugins') }}/datepicker/datepicker-fr.js"></script>
<!-- DatePicker UI -->
<script type="text/javascript" src="{{ asset ('backOffice/plugins') }}/datepicker-ui/datepicker.js"></script>
<!-- FullCalendar -->
<script src="{{ asset ('backOffice/plugins') }}/fullcalendar/lib/moment.min.js"></script>
<script src="{{ asset ('backOffice/plugins') }}/fullcalendar/lib/jquery-ui.custom.min.js"></script>
<script type="text/javascript" src="{{ asset ('backOffice/plugins') }}/fullcalendar/fullcalendar.js"></script>
<script type="text/javascript" src="{{ asset ('backOffice/plugins') }}/fullcalendar/lang-all.js"></script>
<!-- Chosen -->
<script type="text/javascript" src="{{ asset ('backOffice/plugins') }}/chosen/chosen.js"></script>
<!-- Uniform -->
<script type="text/javascript" src="{{ asset ('backOffice/plugins') }}/uniform/uniform.js"></script>

<!-- TouchSpin -->
<script type="text/javascript" src="{{ asset ('backOffice/plugins') }}/touchspin/touchspin.js"></script>

<!-- Wizard -->
<script type="text/javascript" src="{{ asset ('backOffice/plugins') }}/wizard/wizard.js"></script>

<!-- Global Backend JS -->
<script type="text/javascript" src="{{ asset ('backOffice/js/backend.js') }}"></script>

