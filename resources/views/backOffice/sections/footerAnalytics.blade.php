
<script src="{{asset('backOffice/js/app.js')}}"></script>

<script>
  $(function() {
    $("#datatables-dashboard-traffic").DataTable({
      pageLength: 5,
      lengthChange: false,
      bFilter: false,
      autoWidth: false,
      order: [
        [1, "desc"]
      ]
    });

    $("#datatables-dev-student").DataTable({
      pageLength: 5,
      lengthChange: false,
      bFilter: false,
      autoWidth: false,
      order: [
        [1, "desc"]
      ]
    });

    $("#datatables-marketing-student").DataTable({
      pageLength: 5,
      lengthChange: false,
      bFilter: false,
      autoWidth: false,
      order: [
        [1, "desc"]
      ]
    });

    $("#datatables-dev-student-accepted").DataTable({
      pageLength: 10,
      info : false,
      lengthChange: false,
      bFilter: false,
      autoWidth: false,
      order: [
        [1, "desc"]
      ]
    });

    $("#datatables-dev-student-refused").DataTable({
      pageLength: 10,
      info : false,
      lengthChange: false,
      bFilter: false,
      autoWidth: false,
      order: [
        [1, "desc"]
      ]
    });
  });
</script>
