    <title>{{ $title or '' }}</title>
    <meta name="description" content="{{  $description or 'Easy Service Transport' }}">
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <style>
        /* Loading Spinner */
        .spinner {
            margin: 0;
            width: 70px;
            height: 18px;
            margin: -35px 0 0 -9px;
            position: absolute;
            top: 50%;
            left: 50%;
            text-align: center
        }

        .spinner > div {
            width: 18px;
            height: 18px;
            background-color: #333;
            border-radius: 100%;
            display: inline-block;
            -webkit-animation: bouncedelay 1.4s infinite ease-in-out;
            animation: bouncedelay 1.4s infinite ease-in-out;
            -webkit-animation-fill-mode: both;
            animation-fill-mode: both
        }

        .spinner .bounce1 {
            -webkit-animation-delay: -.32s;
            animation-delay: -.32s
        }

        .spinner .bounce2 {
            -webkit-animation-delay: -.16s;
            animation-delay: -.16s
        }

        @-webkit-keyframes bouncedelay {
            0%, 80%, 100% {
                -webkit-transform: scale(0.0)
            }
            40% {
                -webkit-transform: scale(1.0)
            }
        }

        @keyframes bouncedelay {
            0%, 80%, 100% {
                transform: scale(0.0);
                -webkit-transform: scale(0.0)
            }
            40% {
                transform: scale(1.0);
                -webkit-transform: scale(1.0)
            }
        }

        .sfActive
        {
            background: transparent!important;
        }
        #theme-options
        {
            display:none!important;
        }
    </style>
    <meta charset="UTF-8">
    <!--[if IE]>
    <meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link rel="stylesheet" type="text/css" href="{{ asset ('backOffice/plugins') }}/bootstrap/bootstrap.css">

    <!-- HELPERS -->
    <link rel="stylesheet" type="text/css" href="{{ asset ('backOffice/css') }}/animate.css">
    <link rel="stylesheet" type="text/css" href="{{ asset ('backOffice/css') }}/backgrounds.css">
    <link rel="stylesheet" type="text/css" href="{{ asset ('backOffice/css') }}/boilerplate.css">
    <link rel="stylesheet" type="text/css" href="{{ asset ('backOffice/css') }}/border-radius.css">
    <link rel="stylesheet" type="text/css" href="{{ asset ('backOffice/css') }}/grid.css">
    <link rel="stylesheet" type="text/css" href="{{ asset ('backOffice/css') }}/page-transitions.css">
    <link rel="stylesheet" type="text/css" href="{{ asset ('backOffice/css') }}/spacing.css">
    <link rel="stylesheet" type="text/css" href="{{ asset ('backOffice/css') }}/typography.css">
    <link rel="stylesheet" type="text/css" href="{{ asset ('backOffice/css')}}/utils.css">
    <link rel="stylesheet" type="text/css" href="{{ asset ('backOffice/css') }}/colors.css">

    <!-- ELEMENTS -->
    <link rel="stylesheet" type="text/css" href="{{ asset ('backOffice/inc') }}/chat.css">
    <link rel="stylesheet" type="text/css" href="{{ asset ('backOffice/inc') }}/files-box.css">
    <link rel="stylesheet" type="text/css" href="{{ asset ('backOffice/inc') }}/login-box.css">
    <link rel="stylesheet" type="text/css" href="{{ asset ('backOffice/inc') }}/notification-box.css">
    <link rel="stylesheet" type="text/css" href="{{ asset ('backOffice/inc') }}/progress-box.css">
    <link rel="stylesheet" type="text/css" href="{{ asset ('backOffice/inc') }}/todo.css">
    <link rel="stylesheet" type="text/css" href="{{ asset ('backOffice/inc') }}/user-profile.css">
    <link rel="stylesheet" type="text/css" href="{{ asset ('backOffice/inc') }}/mobile-navigation.css">
    <link rel="stylesheet" type="text/css" href="{{ asset ('backOffice/inc') }}/badges.css">
    <link rel="stylesheet" type="text/css" href="{{ asset ('backOffice/inc') }}/buttons.css">
    <link rel="stylesheet" type="text/css" href="{{ asset ('backOffice/inc') }}/content-box.css">
    <link rel="stylesheet" type="text/css" href="{{ asset ('backOffice/inc') }}/dashboard-box.css">
    <link rel="stylesheet" type="text/css" href="{{ asset ('backOffice/inc') }}/forms.css">
    <link rel="stylesheet" type="text/css" href="{{ asset ('backOffice/inc') }}/images.css">
    <link rel="stylesheet" type="text/css" href="{{ asset ('backOffice/inc') }}/info-box.css">
    <link rel="stylesheet" type="text/css" href="{{ asset ('backOffice/inc') }}/invoice.css">
    <link rel="stylesheet" type="text/css" href="{{ asset ('backOffice/inc') }}/loading-indicators.css">
    <link rel="stylesheet" type="text/css" href="{{ asset ('backOffice/inc') }}/menus.css">
    <link rel="stylesheet" type="text/css" href="{{ asset ('backOffice/inc') }}/panel-box.css">
    <link rel="stylesheet" type="text/css" href="{{ asset ('backOffice/inc') }}/response-messages.css">
    <link rel="stylesheet" type="text/css" href="{{ asset ('backOffice/inc') }}/responsive-tables.css">
    <link rel="stylesheet" type="text/css" href="{{ asset ('backOffice/inc') }}/ribbon.css">
    <link rel="stylesheet" type="text/css" href="{{ asset ('backOffice/inc') }}/social-box.css">
    <link rel="stylesheet" type="text/css" href="{{ asset ('backOffice/inc') }}/tables.css">
    <link rel="stylesheet" type="text/css" href="{{ asset ('backOffice/inc') }}/tile-box.css">
    <link rel="stylesheet" type="text/css" href="{{ asset ('backOffice/inc') }}/timeline.css">

    <!-- ICONS -->
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"/>
    <link rel="stylesheet" type="text/css" href="{{ asset ('backOffice/icons') }}/fontawesome/fontawesome.css">
    <link rel="stylesheet" type="text/css" href="{{ asset ('backOffice/icons') }}/linecons/linecons.css">
    <link rel="stylesheet" type="text/css" href="{{ asset ('backOffice/icons') }}/spinnericon/spinnericon.css">
    <link rel="stylesheet" type="text/css" href="{{ asset ('backOffice/icons') }}/iconic/iconic.css">
    <link rel="stylesheet" type="text/css" href="{{ asset ('backOffice/icons') }}/elusive/elusive.css">
    <link rel="stylesheet" type="text/css" href="{{ asset ('backOffice/icons') }}/meteocons/meteocons.css">
    <link rel="stylesheet" type="text/css" href="{{ asset ('backOffice/icons') }}/typicons/typicons.css">

    <!-- PLUGINS -->
    <link rel="stylesheet" type="text/css" href="{{ asset ('backOffice/plugins') }}/accordion-ui/accordion.css">
    <link rel="stylesheet" type="text/css" href="{{ asset ('backOffice/plugins') }}/calendar/calendar.css">
    <link rel="stylesheet" type="text/css" href="{{ asset ('backOffice/plugins') }}/carousel/carousel.css">

    <link rel="stylesheet" type="text/css" href="{{ asset ('backOffice/plugins') }}/justgage/justgage.css">
    <link rel="stylesheet" type="text/css" href="{{ asset ('backOffice/plugins') }}/morris/morris.css">
    <link rel="stylesheet" type="text/css" href="{{ asset ('backOffice/plugins') }}/piegage/piegage.css">
    <link rel="stylesheet" type="text/css" href="{{ asset ('backOffice/plugins') }}/xcharts/xcharts.css">
    <link rel="stylesheet" type="text/css" href="{{ asset ('backOffice/plugins') }}/datatable_frontoffice/datatable.css">
    {{--<link rel="stylesheet" type="text/css" href="{{ asset ('backOffice/plugins') }}/datatable/datatable.css">--}}

    <link rel="stylesheet" type="text/css" href="{{ asset ('backOffice/plugins') }}/chosen/chosen.css">
    <link rel="stylesheet" type="text/css" href="{{ asset ('backOffice/plugins') }}/colorpicker/colorpicker.css">
    <link rel="stylesheet" type="text/css" href="{{ asset ('backOffice/plugins') }}/datepicker/datepicker.css">
    <link rel="stylesheet" type="text/css" href="{{ asset ('backOffice/plugins') }}/datepicker-ui/datepicker.css">
    <link rel="stylesheet" type="text/css" href="{{ asset ('backOffice/plugins') }}/daterangepicker/daterangepicker.css">
    <link rel="stylesheet" type="text/css" href="{{ asset ('backOffice/plugins') }}/dialog/dialog.css">
    <link rel="stylesheet" type="text/css" href="{{ asset ('backOffice/plugins') }}/dropdown/dropdown.css">
    <link rel="stylesheet" type="text/css" href="{{ asset ('backOffice/plugins') }}/dropzone/dropzone.css">
    <link rel="stylesheet" type="text/css" href="{{ asset ('backOffice/plugins') }}/fileinput/fileinput.css">
    <link rel="stylesheet" type="text/css" href="{{ asset ('backOffice/plugins') }}/inputswitch/inputswitch.css">
    <link rel="stylesheet" type="text/css" href="{{ asset ('backOffice/plugins') }}/inputswitch/inputswitch-alt.css">
    <link rel="stylesheet" type="text/css" href="{{ asset ('backOffice/plugins') }}/ionrangeslider/ionrangeslider.css">
    <link rel="stylesheet" type="text/css" href="{{ asset ('backOffice/plugins') }}/jcrop/jcrop.css">
    <link rel="stylesheet" type="text/css" href="{{ asset ('backOffice/plugins') }}/jgrowl/jgrowl.css">
    <link rel="stylesheet" type="text/css" href="{{ asset ('backOffice/plugins') }}/loadingbar/loadingbar.css">
    <link rel="stylesheet" type="text/css" href="{{ asset ('backOffice/plugins') }}/vectormaps/vectormaps.css">
    <link rel="stylesheet" type="text/css" href="{{ asset ('backOffice/plugins') }}/markdown/markdown.css">
    <link rel="stylesheet" type="text/css" href="{{ asset ('backOffice/plugins') }}/modal/modal.css">
    <link rel="stylesheet" type="text/css" href="{{ asset ('backOffice/plugins') }}/multiselect/multiselect.css">
    <link rel="stylesheet" type="text/css" href="{{ asset ('backOffice/plugins') }}/fileupload/fileupload.css">
    <link rel="stylesheet" type="text/css" href="{{ asset ('backOffice/plugins') }}/nestable/nestable.css">
    <link rel="stylesheet" type="text/css" href="{{ asset ('backOffice/plugins') }}/noty/noty.css">
    <link rel="stylesheet" type="text/css" href="{{ asset ('backOffice/plugins') }}/popover/popover.css">
    <link rel="stylesheet" type="text/css" href="{{ asset ('backOffice/plugins') }}/prettyphoto/prettyphoto.css">
    <link rel="stylesheet" type="text/css" href="{{ asset ('backOffice/plugins') }}/progressbar/progressbar.css">
    <link rel="stylesheet" type="text/css" href="{{ asset ('backOffice/plugins') }}/rangeslider/rangeslider.css">
    <link rel="stylesheet" type="text/css" href="{{ asset ('backOffice/plugins') }}/slidebars/slidebars.css">
    <link rel="stylesheet" type="text/css" href="{{ asset ('backOffice/plugins') }}/slider-ui/slider.css">
    <link rel="stylesheet" type="text/css" href="{{ asset ('backOffice/plugins') }}/summernote-wysiwyg/summernote-wysiwyg.css">
    <link rel="stylesheet" type="text/css" href="{{ asset ('backOffice/plugins') }}/tabs-ui/tabs.css">
    <link rel="stylesheet" type="text/css" href="{{ asset ('backOffice/plugins') }}/themeswitcher/themeswitcher.css">
    <link rel="stylesheet" type="text/css" href="{{ asset ('backOffice/plugins') }}/timepicker/timepicker.css">
    <link rel="stylesheet" type="text/css" href="{{ asset ('backOffice/plugins') }}/tocify/tocify.css">
    <link rel="stylesheet" type="text/css" href="{{ asset ('backOffice/plugins') }}/tooltip/tooltip.css">
    <link rel="stylesheet" type="text/css" href="{{ asset ('backOffice/plugins') }}/touchspin/touchspin.css">
    <link rel="stylesheet" type="text/css" href="{{ asset ('backOffice/plugins') }}/uniform/uniform.css">
    <link rel="stylesheet" type="text/css" href="{{ asset ('backOffice/plugins') }}/wizard/wizard.css">
    <link rel="stylesheet" type="text/css" href="{{ asset ('backOffice/plugins') }}/xeditable/xeditable.css">
    <link rel="stylesheet" type="text/css" href="{{ asset ('backOffice/plugins') }}/sweetalert/sweetalert.css">
    <link rel="stylesheet" type="text/css" href="{{ asset ('backOffice/plugins') }}/sweetalert/swal-forms.css">
    <link href="{{ asset ('backOffice/plugins/toastr/toastr.css') }}" rel="stylesheet">

    <!-- Admin theme -->
    <link rel="stylesheet" type="text/css" href="{{ asset ('backOffice/css') }}/layout.css">
    <link rel="stylesheet" type="text/css" href="{{ asset ('backOffice/css') }}/default_colors.css">

    <!-- Components theme -->
    <link rel="stylesheet" type="text/css" href="{{ asset ('backOffice/css') }}/default.css">
    <link rel="stylesheet" type="text/css" href="{{ asset ('backOffice/css') }}/border-radius_002.css">

    <!-- Admin responsive -->
    <link rel="stylesheet" type="text/css" href="{{ asset ('backOffice/css') }}/responsive-elements.css">
    <link rel="stylesheet" type="text/css" href="{{ asset ('backOffice/css') }}/admin-responsive.css">

    <!-- JS Core -->
    <script type="text/javascript" src="{{ asset ('backOffice/plugins') }}/jquery/jquery.js"></script>
    <script type="text/javascript" src="{{ asset ('backOffice/plugins') }}/jquery/jquery-core.js"></script>
    <script type="text/javascript" src="{{ asset ('backOffice/plugins') }}/jquery-ui/jquery-ui-core.js"></script>
    <script type="text/javascript" src="{{ asset ('backOffice/plugins') }}/jquery-ui/jquery-ui-widget.js"></script>
    <script type="text/javascript" src="{{ asset ('backOffice/plugins') }}/jquery-ui/jquery-ui-mouse.js"></script>
    <script type="text/javascript" src="{{ asset ('backOffice/plugins') }}/jquery-ui/jquery-ui-position.js"></script>
    <!--<script type="text/javascript" src="../../abackOffice/ssets/js-core/transition.js"></script>-->
    <script type="text/javascript" src="{{ asset ('backOffice/plugins') }}/modernizr/modernizr.js"></script>
    <script type="text/javascript" src="{{ asset ('backOffice/plugins') }}/jquery/jquery-cookie.js"></script>
    <script type="text/javascript" src="{{ asset ('backOffice/plugins') }}/sweetalert/sweetalert.min.js"></script>
    <script type="text/javascript" src="{{ asset ('backOffice/plugins') }}/toastr/toastr.js"></script>
    <script type="text/javascript" src="{{asset('backOffice/plugins')}}/datatable/datatable.js"></script>
    <script type="text/javascript" src="{{asset('backOffice/plugins')}}/datatable/datatable-bootstrap.js"></script>
    <script type="text/javascript" src="{{asset('backOffice/plugins')}}/datatable/datatable-tabletools.js"></script>

    <script type="text/javascript">
        $(window).load(function () {
            setTimeout(function () {
                $('#loading').fadeOut(400, "linear");
            }, 300);
        });
    </script>
    <!-- Sweet alert -->
    <link rel="stylesheet" href="{{asset('backOffice/manager/bower_components')}}/sweetalert/swal-forms.css">
    <link rel="stylesheet" href="{{asset('backOffice/manager/bower_components')}}/sweetalert/sweetalert.css">
