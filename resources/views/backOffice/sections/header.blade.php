<div id="page-header" class="bg-gradient-9">
    <div id="mobile-navigation">
        <button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"></button>
        <a href="#" class="logo-content-small"></a>
    </div>
    <div id="header-logo" class="logo-bg">
        <a href="#" title="Dashboard">
          <img src="{{asset('assets/logoDipper/logo3.png')}}" class="logoDipper">
        </a>
        <a href="#" class="logo-content-small" title="Accueil">
          <img src="{{asset('assets/logoDipper/logo3.png')}}" class="minLogoDipper" alt="logoDipper">
        </a>
        <a id="close-sidebar" href="#" title="Close sidebar">
            <i class="glyph-icon icon-angle-left"></i>
        </a>
    </div>
    <div id="header-nav-left">
        <div class="user-account-btn dropdown">
            <a href="#" title="Mon Compte" class="user-profile clearfix" data-toggle="dropdown">
                <img width="28" src="{{asset('backOffice/images/unknown.png')}}" alt="Profile picture">
                <span>{{Auth::user()->first_name.' '.Auth::user()->last_name}}</span>
                <i class="glyph-icon icon-angle-down"></i>
            </a>
            <div class="dropdown-menu float-left">
                <div class="box-sm">
                    <div class="login-box clearfix">
                        <div class="user-img">
                            <img src="{{asset('backOffice/images/unknown.png')}}" alt="Profile picture">
                        </div>
                        <div class="user-info">
                            <span>
                                <i> {{Auth::user()->roles->first()->title}}</i>
                            </span>
                            <a href="#" title="Modifier profile">Update Profile</a>
                        </div>
                    </div>
                    <div class="pad5A button-pane button-pane-alt text-center">
                        <a href="{{route('handleDashboardLogout')}}" class="btn display-block font-normal btn-danger">
                            <i class="glyph-icon icon-power-off"></i>Logout
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- #header-nav-left -->
    <div id="header-nav-right">
        <a href="#" class="hdr-btn" id="fullscreen-btn" title="Full Screen">
            <i class="glyph-icon icon-arrows-alt"></i>
        </a>
        <a class="header-btn" id="logout-btn" href="{{route('handleDashboardLogout')}}" title="Logout">
            <i class="glyph-icon icon-power-off"></i>
        </a>
    </div>
    <!-- #header-nav-right -->
</div>
