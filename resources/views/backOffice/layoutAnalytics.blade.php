<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <link rel="icon" type="image/png" href="{{asset('')}}" />
      @include('backOffice.sections.head')
    @include('backOffice.sections.headAnalytics')

</head>
<body>
{{--@include('sweet::alert')--}}
<div id="sb-site">
    @include('backOffice.sections.slideBar')
    <div id="page-wrapper">

    @include('backOffice.sections.header')
    </div>
    @include('backOffice.sections.sideBar')
    <div id="page-content-wrapper">
        <div id="page-content">
    @yield('content')
        </div>
    </div>

</div>
@yield('backOffice.sections.scripts')
@include('backOffice.sections.footer')
@include('backOffice.sections.footerAnalytics')
</body>
</html>
