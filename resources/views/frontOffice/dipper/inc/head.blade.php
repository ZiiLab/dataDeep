
<meta http-equiv="content-type" content="text/html; charset=UTF-8">

@yield('title')
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width,initial-scale=1.0">
<meta name="csrf-token" content="{{ csrf_token() }}">
<meta name="description" content="Your gorgeous description">
<link rel="stylesheet" href="{{asset('dipper/fonts/opensans/stylesheet.css')}}" async>
<link rel="stylesheet" href="{{asset('dipper/fonts/TTCommons/stylesheet.css')}}" async>
<link rel="stylesheet" href="{{asset('dipper/fonts/ionicons/ionicons.min.css')}}" async>
<link rel="stylesheet" href="{{asset('dipper/fonts/fontawesome/font-awesome.min.css')}}" async>

<link rel="stylesheet" href="{{asset('dipper/css/pageloader.css')}}" async>

<link rel="stylesheet" href="{{asset('dipper/css/bootstrap.min.css')}}" async>
<link rel="stylesheet" href="{{asset('dipper/js/vendor/swiper.min.css')}}" async>
<link rel="stylesheet" href="{{asset('dipper/js/vendor/jquery.fullpage.min.css')}}" async>
<link rel="stylesheet" href="{{asset('dipper/js/vegas/vegas.min.css')}}" async>

<link rel="stylesheet" href="{{asset('dipper/css/main.css')}}" async>
<link rel="stylesheet" href="{{asset('dipper/css/style-gradient.css')}}" async>

<link href="https://fonts.googleapis.com/css?family=Archivo+Black&display=swap" rel="stylesheet" async>

<link rel="stylesheet" href= "https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.9.0/css/all.css" async> 

{{-- <link href="https://www.fontify.me/wf/822845a211e250d3989a107203e72537" rel="stylesheet" type="text/css"> --}}

<link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i&display=swap" rel="stylesheet" async>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-153582180-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-153582180-1');
</script>
<!-- End Google Tag Manager -->

<!-- <script async src="https://www.googletagmanager.com/gtag/js?id=UA-153511284-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-153511284-1');
</script> -->
