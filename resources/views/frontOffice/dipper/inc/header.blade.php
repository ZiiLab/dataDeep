<header class="page-header navbar page-header-alpha scrolled-white menu-right topmenu-right">

    <button class="navbar-toggler site-menu-icon" id="navMenuIcon">

        <span class="menu-icon menu-icon-random">
            <span class="bars">
                <span class="bar1"></span>
                <span class="bar2"></span>
                <span class="bar3"></span>
            </span>
        </span>
    </button>


    <a class="navbar-brand" href="#home">
        <span class="logo">
            <img class="light-logo" src="{{asset('assets/logoDipper/logo3.png')}}" alt="Logo">
        </span>
    </a>


    <div class="all-menu-wrapper" id="navbarMenu">

        <nav class="navbar-mainmenu">
            <ul class="navbar-nav mr-auto mr-auto-my">
                <li class="nav-item active">
                    <a class="nav-link" href="{{route('showHome')}}#home">Home
                        <span class="sr-only">(current)</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{route('showGallery')}}">Articles</a>
                </li>

            </ul>
        </nav>


        <nav class="navbar-sidebar" id="navbar-sidebar">
            <ul class="navbar-nav" id="qmenu">
                <li class="nav-item" data-menuanchor="home">
                    <a href="#home">
                        {{-- <i class="icon ion-ios-home"></i> --}}
                        <span class="txt">Home</span>
                    </a>
                </li>
                <li class="nav-item" data-menuanchor="about">
                    <a href="#about">
                        {{-- <i class="icon ion-star"></i> --}}
                        <span class="txt">About</span>
                    </a>
                </li>
                <li class="nav-item" data-menuanchor="services">
                    <a href="#services">
                        {{-- <i class="icon ion-clipboard"></i> --}}
                        <span class="txt">Services</span>
                    </a>
                </li>
                <li class="nav-item" data-menuanchor="pricelist">
                    <a href="#pricelist">
                        {{-- <i class="icon ion-cash"></i> --}}
                        <span class="txt">Packages</span>
                    </a>
                </li>
                <li class="nav-item" data-menuanchor="portfolio">
                    <a href="#portfolio">
                        {{-- <i class="icon ion-ios-briefcase"></i> --}}
                        <span class="txt">Portfolio</span>
                    </a>
                </li>
                <li class="nav-item" data-menuanchor="blog">
                    <a href="#blog">
                        {{-- <i class="icon ion-cube"></i> --}}
                        <span class="txt">Blog</span>
                    </a>
                </li>
                <li class="nav-item" data-menuanchor="contact">
                    <a href="#contact">
                        {{-- <i class="icon ion-email"></i> --}}
                        <span class="txt">Contact</span>
                    </a>
                </li>
            </ul>
        </nav>

    </div>

</header>
