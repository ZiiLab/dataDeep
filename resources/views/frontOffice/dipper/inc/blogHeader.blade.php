<style media="screen">
body {
  background-color: rgba(0, 0, 0, 0.1) !important;

}
</style>
<header class="page-header navbar page-header-alpha scrolled-white menu-right topmenu-right">

  <button class="navbar-toggler site-menu-icon" id="navMenuIcon">

    <span class="menu-icon menu-icon-random">
      <span class="bars">
        <span class="bar1"></span>
        <span class="bar2"></span>
        <span class="bar3"></span>
      </span>
    </span>
  </button>


  <a class="navbar-brand" href="{{route('showHome')}}#home">
    <span class="logo">
      <img class="light-logo" src="{{asset('assets/logoDipper/logo3.png')}}" alt="Logo">
    </span>
  </a>


  <div class="all-menu-wrapper" id="navbarMenu">

    <nav class="navbar-mainmenu">
      <ul class="navbar-nav mr-auto mr-auto-my">
        <li class="nav-item active">
          <a class="nav-link" href="{{route('showHome')}}#home">Home
            <span class="sr-only">(current)</span>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="{{route('showGallery')}}">Articles</a>
        </li>

      </ul>
    </nav>


    <nav class="navbar-sidebar ">
      <ul  id="filterNav">
        <li data-filter="*" class="nav-item" data-menuanchor="home">
          <a href="#home" class="category"  data-category="all" data-offset="0">

            <span class="txt">All</span>
          </a>
        </li>
      @foreach($categories as $category)
        <li data-filter="*" class="nav-item" data-menuanchor="home">
          <a href="#home" class="category"  data-category="{{$category->id}}" data-offset="0">

            <span class="txt">{{$category->name}}</span>
          </a>
        </li>
      @endforeach

      </ul>
    </nav>

  </div>

</header>
