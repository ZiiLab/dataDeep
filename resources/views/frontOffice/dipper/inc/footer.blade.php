<style media="screen">
  .social img{
    width: 30px;
    display: block;
  }
</style>
<footer id="site-footer" class="page-footer">

  <div class="footer-right">

    <ul class="social">
      <li>
        <a href="https://www.facebook.com/itsdatadeep/?eid=ARC5YGql_Ipwubh-_smjhSzy5MA7FqT7WPfKcRvdkZmeUGn0DN5HWdtN2_DvLOHjwynGbSTZNMbAlcCB" target="_blank">
          <img src="{{asset('dipper/icons/fb_icon.png')}}" alt="datadeep_fb">
        </a>
      </li>
      <li>
        <a href="https://dribbble.com/datadeep" target="_blank">
          <img src="{{asset('dipper/icons/dribbble_icon.png')}}" alt="datadeep_dribbble">
        </a>
      </li>
      <li>
        <a href="#" target="_blank">
            <img src="{{asset('dipper/icons/twitter_icon.png')}}" alt="datadeep_twitter">
        </a>
      </li>
      <li>
        <a href="https://www.instagram.com/itsdatadeep/?hl=en&fbclid=IwAR1r-D-MEtaPTOeZPouaZJJcRCSk0p8kCg16dlteyay_-wz7islTq2LGWy4" target="_blank">
            <img src="{{asset('dipper/icons/instagram_icon.png')}}" alt="datadeep_instagram">
        </a>
      </li>
      <li>
        <a href="https://www.linkedin.com/company/data-deep?trk=organization-update_share-update_actor-text" target="_blank">
            <img src="{{asset('dipper/icons/linkdin_icon.png')}}" alt="datadeep_linkdin">
        </a>
      </li>
    </ul>
  </div>
</footer>
