
<header class="page-header navbar page-header-alpha scrolled-white menu-right topmenu-right" style="box-shadow:none!important">



    @auth
    <div class="navbar-toggler site-menu-icon user-profile">

      <div class="dropdown">
          <a id="test" href="#" class="dropbtn">
            <img src="{{asset(Auth::user()->image)}}" alt="user-profile">
          </a>
          <div id="myDropdown" class="dropdown-content" style="text-align:center">
              <a href="{{route('handleDashboardLogout')}}">Sign Out <i class="fa fa-sign-out" aria-hidden="true"></i></a>
          </div>
      </div>

    </div>
    @endauth
    <button class="navbar-toggler site-menu-icon pr-100" id="navMenuIcon" >
      <span class="menu-icon menu-icon-random">
        <span class="bars">
          <span class="bar1"></span>
          <span class="bar2"></span>
          <span class="bar3"></span>
        </span>
      </span>
    </button>

    <div class="all-menu-wrapper" id="navbarMenu">

      <nav class="navbar-mainmenu">
        <ul class="navbar-nav mr-auto mr-auto-my">
          <li class="nav-item active">
            <a class="nav-link" href="{{route('showHome')}}#home" >Home
              <span class="sr-only">(current)</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{route('showGallery')}}" >Articles</a>
          </li>

        </ul>
      </nav>
    </div>

    <a class="navbar-brand" href="{{route('showGallery')}}">
        <span class="logo">
            <img class="light-logo" src="{{asset('assets/logoDipper/logo3.png')}}" alt="Logo">
        </span>
    </a>


</header>
