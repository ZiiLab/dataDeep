<!DOCTYPE html >
<html lang="en">

<head>
    @include('frontOffice.dipper.inc.head')
    @yield('css')
</head>


<body id="menu" class="body-page">

  <!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5SVFTD7"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
  <!--[if lt IE 8]>
<p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade
    your browser</a> to improve your experience.</p>
<![endif]-->
    @include('frontOffice.dipper.inc.loader')
    @include('frontOffice.dipper.inc.header')
    @yield('content')

    @include('frontOffice.dipper.inc.footer')
    @include('frontOffice.dipper.inc.scripts')

    @yield('js')
</body>
</html>
