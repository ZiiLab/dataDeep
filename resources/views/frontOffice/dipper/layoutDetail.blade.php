<!DOCTYPE html >
<html lang="en">

<head>
    @include('frontOffice.dipper.inc.head')
    @yield('meta')
    @yield('css')
</head>

<body id="menu" class="body-item">

<!-- End Google Tag Manager (noscript) -->
  <!--[if lt IE 8]>
<p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade
    your browser</a> to improve your experience.</p>
<![endif]-->
    @include('frontOffice.dipper.inc.loader')
    @include('frontOffice.dipper.inc.articleHeader')
    @yield('content')

    @include('frontOffice.dipper.inc.footer')
    @include('frontOffice.dipper.inc.scripts')

    @yield('js')
</body>

</html>
