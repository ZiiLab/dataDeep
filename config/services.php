<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => env('SES_REGION', 'us-east-1'),
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],

    'facebook' => [
        'client_id' => '775054556292917',
        'client_secret' => 'c4e9759737eb11dc3f65b329880b4f9f',
        'redirect' => env('FB_PROVIDER_REDIRECT', 'http://localhost/DataDeep/public/user/social/facebook/callback'),
        ],

    'google' => [
        'client_id' => '832667397213-vhgsr9vekppm6lkfn3c20nup73c17lih.apps.googleusercontent.com',
        'client_secret' => 'NR0-VAPvfqJ5WXevo4k9fDXW',
        'redirect' => env('GOOGLE_PROVIDER_REDIRECT', 'http://localhost/DataDeep/public/user/social/google/callback'),
    ],

];
