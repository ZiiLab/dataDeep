<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\Modules\Blog\Models\Role;
use App\Modules\Blog\Models\User;
use App\Modules\Blog\Models\Article;
use App\Modules\Blog\Models\Category;
use App\Modules\Blog\Models\Click;
use App\Modules\Blog\Models\Comment;
use App\Modules\General\Models\Student;



class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        $faker = Faker::create();

        foreach (range(1,3) as $index) {
            Role::create([
                'title' => $faker->word,
            ]);
        }

        // foreach (range(1,3) as $index) {
        //  $user =  User::create([
        //         'first_name' => $faker->firstName,
        //         'last_name' => $faker->lastName,
        //         'email' => $faker->email,
        //         'password' => bcrypt('secret'),
        //         'status' => 1,
        //     ]);
        //  $user->roles()->attach($index);
        // }

        foreach (range(1,30) as $index) {
          $student = Student::create([
           'email'             => $faker->email,
           'full_name'         => $faker->firstName,
           'age'               => '20',
           'phone'             =>'20622087',
           'address'           => '49',
           'profession'        => 'Student',
           'formation_type'    => $faker->numberBetween(0,1),
           'status'            => 0
         ]);

         $student->skills()->create([
           'label' => 'html'
         ]);

         $student->skills()->create([
           'label' => 'php'
         ]);

         $student->skills()->create([
           'label' => 'css'
         ]);
        }


        // foreach (range(1,5) as $index) {
        //     Category::create([
        //         'name' => $faker->word,
        //         'description' => $faker->sentence($nbWords = 8, $variableNbWords = true),
        //     ]);
        // }
        //
        // foreach (range(1,10) as $index) {
        //     Article::create([
        //         'title' => $faker->sentence($nbWords = 4, $variableNbWords = true),
        //         'description' => $faker->sentence($nbWords = 8, $variableNbWords = true),
        //         'content' => $faker->paragraph($nbSentences = 6, $variableNbSentences = true) ,
        //         'image' =>  $faker->imageUrl($width = 640, $height = 480),
        //         'user_id' => 1,
        //         'category_id' => 1,
        //         'status' => 1
        //     ]);
        // }
        //
        //
        // foreach (range(1,20) as $index) {
        //     Comment::create([
        //         'content' => $faker->sentence($nbWords = 8, $variableNbWords = true),
        //         'user_id' => 1,
        //         'article_id' => 1,
        //         'comment_id' => 1,
        //     ]);
        // }
        //
        //
        // foreach (range(1,5) as $index) {
        //     Click::create([
        //         'user_id' => 1,
        //         'article_id' => 1,
        //     ]);
        // }

    //
    // $comment1 = Comment::create([
    //   'content'   => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
    //   'article_id'   =>11,
    //   'user_id'   =>1,
    //   'comment_id'   =>null
    // ]);
    //
    // $comment2 = Comment::create([
    //   'content'   => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit',
    //   'article_id'   => 11,
    //   'user_id'   =>2,
    //   'comment_id'   =>null
    // ]);
    //
    // $comment3 = Comment::create([
    //   'content'   => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor et dolore magna aliqua.',
    //   'article_id'   => 11,
    //   'user_id'   => 3,
    //   'comment_id'   =>null
    // ]);
    //
    // Comment::create([
    //   'content'   => 'Lorem ipsum dolor sit amet.',
    //   'article_id'   =>null,
    //   'user_id'   =>2,
    //   'comment_id'   =>$comment1->id
    // ]);
    //
    // Comment::create([
    //   'content'   => 'Lorem ipsum dolor sit amet.',
    //   'article_id'   =>null,
    //   'user_id'   =>3,
    //   'comment_id'   =>$comment1->id
    // ]);


    }
}
