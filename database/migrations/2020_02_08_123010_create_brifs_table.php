<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBrifsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('general_infos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('ref');
            $table->string('nom')->nullable();
            $table->string('address')->nullable();
            $table->string('phone')->nullable();
            $table->string('fax')->nullable();
            $table->string('email')->nullable();
            $table->string('activity')->nullable();
            $table->timestamps();
        });
        Schema::create('personal_infos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nom')->nullable();
            $table->string('position')->nullable();
            $table->string('phone')->nullable();
            $table->string('email')->nullable();
            $table->integer('business_id')->unsigned()->nullable();
            $table->foreign('business_id')->references('id')->on('general_infos')->onDelete('cascade');
            $table->timestamps();
        });

        Schema::create('portfolios', function (Blueprint $table) {
            $table->increments('id');
            $table->string('service')->nullable();
            $table->string('va')->nullable();
            $table->string('values')->nullable();
            $table->string('audience')->nullable();
            $table->string('market')->nullable();
            $table->string('concurrents')->nullable();
            $table->integer('business_id')->unsigned()->nullable();
            $table->foreign('business_id')->references('id')->on('general_infos')->onDelete('cascade');
            $table->timestamps();
        });

        Schema::create('managements', function (Blueprint $table) {
            $table->increments('id');
            $table->string('existent_accounts')->nullable();
            $table->string('needed_accounts')->nullable();
            $table->string('other_accounts')->nullable();
            $table->integer('max_posts')->nullable();
            $table->string('post_types')->nullable();
            $table->string('other_types')->nullable();
            $table->string('preferred_supports')->nullable();
            $table->tinyInteger('sponsored')->nullable(); // 0 no / 1 yes /2 maybe
            $table->string('sponsored_supports')->nullable();
            $table->string('sponsor_goal')->nullable();
            $table->string('contract_delay')->nullable();
            $table->string('reporting_delay')->nullable();
            $table->integer('business_id')->unsigned()->nullable();
            $table->foreign('business_id')->references('id')->on('general_infos')->onDelete('cascade');
            $table->timestamps();
        });

        Schema::create('shootings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('type')->nullable();
            $table->string('photo_types')->nullable();
            $table->string('categories')->nullable();
            $table->integer('number')->nullable();
            $table->string('theme')->nullable();
            $table->string('length')->nullable();
            $table->integer('business_id')->unsigned()->nullable();
            $table->foreign('business_id')->references('id')->on('general_infos')->onDelete('cascade');
            $table->timestamps();
        });

        Schema::create('developments', function (Blueprint $table) {
            $table->increments('id');
            $table->tinyInteger('have')->nullable(); // 0 no / 1 yes
            $table->tinyInteger('create')->nullable(); // 0 no / 1 yes
            $table->tinyInteger('moderate')->nullable(); // 0 no / 1 yes
            $table->string('url')->nullable();
            $table->string('techs')->nullable();
            $table->string('type')->nullable();
            $table->tinyInteger('domain')->nullable(); // 0 no / 1 yes

            $table->string('domain_name')->nullable();
        
            $table->tinyInteger('hosting')->nullable(); // 0 no / 1 yes

            $table->string('hosting_name')->nullable();
            $table->text('pages')->nullable();
            $table->string('languages')->nullable();
            $table->tinyInteger('content')->nullable(); // 0 we / 1 others
            $table->string('content_type')->nullable();
            $table->string('theme')->nullable();
            $table->integer('business_id')->unsigned()->nullable();
            $table->string('other_languages')->nullable();
            $table->string('other_site_type')->nullable();
            $table->string('other_pages')->nullable();
            $table->foreign('business_id')->references('id')->on('general_infos')->onDelete('cascade');
            $table->timestamps();
        });

        Schema::create('graphics', function (Blueprint $table) {
            $table->increments('id');
            $table->tinyInteger('logo')->nullable(); // 0 no / 1 yes / 2 maybe
            $table->tinyInteger('cards')->nullable(); // 0 no / 1 yes / 2 maybe
            $table->tinyInteger('flyers')->nullable(); // 0 no / 1 yes / 2 maybe
            $table->tinyInteger('catalogue')->nullable(); // 0 no / 1 yes / 2 maybe
            $table->tinyInteger('chart')->nullable(); // 0 no / 1 yes / 2 maybe
            $table->tinyInteger('ppt')->nullable(); // 0 no / 1 yes / 2 maybe
            $table->string('affiche')->nullable();
            $table->integer('business_id')->unsigned()->nullable();
            $table->foreign('business_id')->references('id')->on('general_infos')->onDelete('cascade');
            $table->timestamps();
        });
        Schema::create('approvals', function (Blueprint $table) {
            $table->increments('id');
            $table->dateTime('start')->nullable();
            $table->dateTime('end')->nullable();
            $table->string('approved_by')->nullable();
            $table->integer('business_id')->unsigned()->nullable();
            $table->foreign('business_id')->references('id')->on('general_infos')->onDelete('cascade');
            $table->timestamps();
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
}
