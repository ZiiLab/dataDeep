<?php

namespace App\Modules\General\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modules\Blog\Models\Article;
use App\Modules\Blog\Models\User;
use App\Modules\Blog\Models\Category;
use App\Modules\Blog\Models\Comment;
use App\Modules\General\Models\Subscribe;
use App\Modules\General\Models\Contact;
use App\Modules\General\Models\Student;
use Laravel\Socialite\Facades\Socialite;
use Illuminate\Support\Facades\Mail;
use Auth;
use Session;


use Illuminate\Support\Facades\Validator;

class GeneralController extends Controller
{


  public function showBriefClient()
  {
    return view('General::brief');
  }
  // 
  // public function showResumeClient()
  // {
  //     return view('General::resume');
  // }

  public function showHome()
    {

        return view("General::index",[
          'articles' => Article::orderBy('created_at', 'desc')->take(2)->get()
        ]);
    }

    public function showGallery()
    {
        return view("General::gallery",[
          'articles' => Article::skip(0)->take(6)->get(),
          'categories' => Category::all()
        ]);
    }

    public function showArticle($articleId)
    {

    // dd(Auth::id());

        $id = explode('_',$articleId);

        if(count($id) <2){
           //not found
            return back();
        }

        $article = Article::find($id[1]);

        if(!$article){
            //notfound
            return back();
        }

        $previous = Article::where('id', '<', $article->id)->max('id');
        $next = Article::where('id', '>', $article->id)->min('id');

        $previousArticle = Article::find($previous);
        $nextArticle = Article::find($next);

        return view('General::details',[
            'previousArticle' => $previousArticle,
            'nextArticle' => $nextArticle,
            'article'   => $article,
            'previous'  => $previous,
            'next'      => $next
        ]);
    }


    public function showNextArticle($articleId)
    {

          $id = explode('_',$articleId);

          if(count($id) <2){
             //not found
              return back();
          }
          $article = Article::find($id[1]);
          if(!$article){
              //notfound
              return back();
          }



        return redirect()->route('showArticle','_'.$id);
    }

    public  function apiHandleAddComment(Request $request)
    {
          $data = $request->data;
          $comment = Comment::create([
          'content' => $data['content'],
          'article_id' => isset($request->data['articleId']) ? $request->data['articleId'] : null,
          'user_id' => Auth::id(),
          'comment_id'=> isset($request->data['commentId']) ? $request->data['commentId'] : null
        ]);

        if(!$comment){
          return response()->json(['status' => 400]); //somethin went wrong
         }

      return response()->json(['status' => 200 , 'comment' => $comment ]);
    }

    public function apiHandleShowMoreComment(Request $request)
    {



        $data = $request->data;

        $comments = Comment::where('article_id',$request->data['articleId'])->skip($request->data['offset']*3)->take(3)->with('author')->with('comments')->get();

        if(count($comments)<0){
          return response()->json(['status' => 200,'event'=>flase]); //somethin went wrong
         }

      return response()->json(['status' => 200 , 'comments' => $comments,'row' => count($comments) ]);
    }


    public function apiHandleGetCommentReplies(Request $request)
    {

        $data = $request->data;


      $replies =   Comment::find($data['commentId'])->comments()->with('author')->get();

        // $replies = Comment::where('comment_id',$data['commentId'])->comments()->with('author');

        return response()->json(['status' => 200 , 'replies' => $replies ]);

    }



    public function handleProviderRedirect($provider)
    {
         $url = url()->previous();
         $id = explode('_',$url)[1];
         Session::put("articleId",$id);

        return Socialite::driver($provider)->redirect();
    }

    public function handleProviderCallback($provider)
    {
        $providerData = Socialite::driver($provider)->stateless()->user();

        $articleId = Session::get('articleId');

       // dd($articleId);

        $user = User::where('provider_id', '=', $providerData->id)
            ->orWhere('email', '=', $providerData->email)
            ->first();

        if (!$user) {

            if ($providerData->name == null) {
                $username = strtok($providerData->email, '@');
            } else {
                $username = $providerData->name;
            }

            $imagePath = 'storage/uploads/avatar/'.$providerData->id.'-'.time().'.png';
            file_put_contents($imagePath, file_get_contents($providerData->avatar));

            $user = User::create([
                'provider_id' => $providerData->id,
                'provider' => $provider,
                'email' => $providerData->email,
                'first_name' => $username,
                'last_name' => '',
                'image'  => $imagePath ,
                'status' => 1

            ]);
            $content = [$user->email];

             Mail::send('General::mail.welcome', $content, function ($message) use ($user) {
                 $message->to($user->email);
                 $message->subject('Welcome to Data Deep');
             });

            Auth::login($user);
          // logged
            return redirect()->route('showArticle','_'.$articleId);
        }

        Auth::login($user);
      //already logged
            return redirect()->route('showArticle','_'.$articleId);
         }


         public function apiHandleUserSubscribe(Request $request)
         {

           $data  = $request['data'];

           $rules = [
            'email' => 'required|email',
           ];

           $messages = [
             'email.required'          => 'Email is required !',
             'email.email'             => 'Invalid Email !'
           ];

           $validation = Validator::make($data, $rules, $messages);

           if ($validation->fails()) {
               return response()->json(['status'=>202,'errors' => $validation->errors()]);
           }

          Session::put('subscribe',true);
          $checkSubscription = Subscribe::where('email',$request->data['email'])->first();

          if ($checkSubscription) {
            return response()->json(['status'=>201]);
          }

          Subscribe::create([
            'email' =>  $request->data['email']
          ]);

          return response()->json(['status'=>200]);
         }



         public function apiHandleShowMoreArticle(Request $request)
         {

         }


        public function apiHandleGategoryFilter(Request $request)
        {


             $categoryId = $request->data['categoryId'];
             $offset = $request->data['offset'];

             $count = count(Article::All());

              if ($offset > ceil($count / 6)) {

                  return response()->json(['status' => '201']);
              }

             if ($categoryId == 'all') {
               $articles = Article::skip($offset * 6)->take(6)->get();

             }else {
               $articles = Article::where('category_id',$categoryId)->skip($offset * 6)->take(6)->get();

             }
            $countResult = count($articles);
             $view = view('General::filterArticle', [
                 'articles' => $articles,
             ])->render();

             return response()->json(['html' => $view,'rows' => $countResult]);
          }


          public function apiSendMessage(Request $request)
          {

              if(!$request->data['name'] || $request->data['name'] === '') {
                return response()->json(['status' => 201]);

              }

              if(!$request->data['email'] || $request->data['email'] === '') {
                return response()->json(['status' => 202]);

              }
              if(!$request->data['message'] || $request->data['message'] === '') {
                return response()->json(['status' => 203]);

              }

              Contact::create([
                'name' => $request->data['name'],
                'email' => $request->data['email'],
                'message' => $request->data['message'],
                'service' => $request->data['service'] ? $request->data['service'] : null,
                'company' => $request->data['company'] ? $request->data['company'] : null,
                'user_id' => Auth::check() ? Auth::id() : null
              ]);

              return response()->json(['status' => 400]);
          }

          public function showEmailTest() {
            return view("General::mail.welcome");
          }


          public function apiCotchingIntegrationInscription(Request $request)
          {
              $data = $request->data;


                  $rules = [
                              'email' => 'required|email',
                              'first_name' => 'required',
                              'last_name' => 'required',
                              'phone' => 'required|numeric|digits:8'
                          ];
                      $messages = [
                          'email.required'          => 'Email is required !',
                          'email.email'             => 'Invalid Email !','
                          first_name.required'          => 'First Name is required !',
                         'last_name.required'          => 'Last Name is required !',
                          'phone.required'        =>    'Phone Number is required!',
                          'phone.numeric'         =>    'Phone Number must be numeric!',
                          'phone.digits'          =>    'Phone must contain 8 Digits!',
                      ];
                    $validation = Validator::make($request->data, $rules, $messages);

                      if ($validation->fails()) {
                          return response()->json(['errors' => $validation->errors()]) ;
                      }

                      // sendMail('User::frontOffice.mails.activation',
                        // $request->data['email'],
                        // ['email' => $request->data['email'], 'validationStr' => $validationStr] ,
                        // 'Account Activation');


                        return response()->json(['status' =>200]) ;
          }

          public function apiCotchingDevInscription(Request $request)
          {
                $data = $request->data;



                $rules = [
                            'email'       => 'required|email',
                            'full_name'  => 'required',
                            'age'         => 'required|numeric|digits:2',
                            'phone'       => 'required|numeric|digits:8',
                            'address'     => 'required',
                            'profession'  => 'required'
                        ];
                    $messages = [
                          'email.required'          => 'Email is required !',
                          'email.email'             => 'Invalid Email !',
                          'first_name.required'     => 'First Name is required !',

                          'phone.required'          =>    'Phone Number is required!',
                          'phone.numeric'           =>    'Phone Number must be numeric!',
                          'phone.digits'            =>    'Phone must contain 8 Digits!',
                          'age.required'            =>    'Age is required!',
                          'age.numeric'             =>    'Age must be numeric!',
                          'age.digits'              =>    'Invalide Age!',
                          'address.digits'          =>    'Address is required!',
                          'profession.required'     =>    'Profession is required !',
                    ];
                  $validation = Validator::make($request->data, $rules, $messages);

                    if ($validation->fails()) {
                        return response()->json(['errors' => $validation->errors()]) ;
                    }



                    $student = Student::create([
                      'email'             => $data['email'],
                      'full_name'         => $data['full_name'],
                      'age'               => $data['age'],
                      'phone'             => $data['phone'],
                      'address'           => $data['address'],
                      'profession'        => $data['profession'],
                      'formation_type'     => $data['formation_type'],
                      'status'            => 0,
                      'user_id'           => Auth::user() ? Auth::user()->id : null
                    ]);

              return response()->json(['status' =>200 , 'studentId' => $student->id]) ;
          }

          public function apiCotchingStudentSkills(Request $request)
          {
                  $data = $request->data;

                  $studentId = $data['studentId'];


                  $student = Student::find($studentId);

                  if (!$student) {
                      return response()->json(['status',401]); //Some thing wont wrong
                  }

                  foreach ($data['skills'] as $key => $value) {
                    $student->skills()->create([
                      'label' => $value
                    ]);
                  }

              return response()->json(['status' =>200]) ;

          }

}
