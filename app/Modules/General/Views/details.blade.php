@extends('frontOffice.dipper.layoutDetail')
@section('meta')
     <meta property="og:url" content="{{route('showArticle',htmlentities(urlencode(str_replace(' ', '-', $article->title))).'_'.$article->id)}}">
     <meta property="og:site_name" content="{{$article->title}}">
     <meta property="og:image" content="{{asset($article->image)}}">
     <meta property="og:image:type" content="image/png/jpg/jpeg">
     <meta property="twitter:creator" content="{{$article->author->first_name.' '.$article->author->last_name}}">
    <meta property="twitter:card" content="summary_large_image">
    <meta property="twitter:title" content="{{$article->title}}">
    <meta property="twitter:description" content="{{$article->description}}">
    <meta property="twitter:image:src" content="{{asset($article->image)}}">
    <meta property="twitter:image:width" content="1291">
    <meta property="twitter:image:height" content="315">
    <meta name="msapplication-TileColor" content="#2A5977">
    <meta name="theme-color" content="#2A5977">

@endsection
@section('title')
      <title>{{$article->title}}</title>
@endsection
  @section('css')
      <link rel="stylesheet" href="{{asset('dipper/css/personalizeDetail.css')}}">
  @endsection
@section('content')


  <style media="screen">

  blockquote {
  background: rgba(255, 214, 0, 0.2);
  border-left: 10px solid rgba(255, 214, 0, 1);
  /* margin: 1.5em 10px; */
  padding: 0.5em 10px;
  quotes: "\201C""\201D""\2018""\2019";
  margin-top: 20px !important;
}
blockquote:before {
  color: rgba(255, 214, 0, 1);
  content: open-quote;
  font-size: 4em;
  line-height: 0.1em;
  margin-right: 0.25em;
  vertical-align: -0.4em;
}
blockquote p {
  display: inline;
}

.share-icons a.fa {
padding: 10px !important;
font-size: 30px !important;
width: 50px !important;
text-align: center !important;
text-decoration: none !important;
margin: 5px 2px !important;
}

#share-icons a.fa{
  color: white;
}

.fa:hover {
  opacity: 0.7;
}

.fa-facebook {
background: #3B5998;
color: white;
}

.fa-twitter {
background: #55ACEE;
color: white;
}

.fa-google {
background: #dd4b39;
color: white;
}

.fa-linkedin {
background: #007bb5;
color: white;
}

.fa-pinterest {
background: #cb2027;
color: white;
}

.article-content img{
    width: 100% !important;
}

.article-content p,.article-content div {
  font-family: 'Roboto', sans-serif !important;
  color: black;
  font-weight: normal;
  /* color: black !important; */
  font-size: 1rem;
}

.article-content ul li strong{

  font-family: 'Roboto', sans-serif !important;
  font-weight: bold !important;
  font-size: 1rem !important;

}


  </style>

<div class="page-cover">

    {{-- <div class="cover-bg bg-img" data-image-src="{{asset('dipper/img/about.jpg')}}"></div> --}}

    <div id="particles-js" class="cover-bg pos-abs full-size bg-color"></div>
</div>

<main  class="page-main page-fullpage main-anim detail-main" id="itempage">


      <div class="section section-twoside fp-auto-height-responsive section-gallery fullheight" data-section="item-alpha">
          <div class="section-cover-tier mask-black" style="background-image:url('{{asset($article->image)}}')">
              <div class="cover-content center-vh text-center padding-cover">

                <div class="container">
                  <div class="row">
                    <div class="title-desc">
                        <h2 class="display-4 display-title title-size" >{{$article->title}}</h2>
                        <p  class="description-size">{{$article->description}}</p>
                    </div>
                  </div>
                </div>

              </div>


              <div class="container">
                <div class="row">
                    <div class="media media-article-title" style="z-index:2;margin-left:25px;align-items: center;color:white;">
                        <img class="media-image d-flex align-self-center mr-3 rounded-circle avatar" src="{{asset($article->author->image ? $article->author->image : 'backOffice/images/unknown.png')}}" alt="author">
                        <div class="media-body align-self-center">
                            <div class="mt-0 media-title"style="margin: 0px;font-size: 13px;">
                                <strong>{{$article->author->first_name.' '.$article->author->last_name}}</strong>

                            </div>
                            <span>  {{ \Carbon\Carbon::parse($article->created_at)->diffForHumans() }}</span>

                        </div>
                    </div>
                </div>
              </div>


          </div>

          <div class="section-wrapper largewidth">


              <div class="item row">

                  <div class="col-12 col-md-12 center-vh">

                      <div class="section-content anim">
                          <article class="article article-light">

                              {{-- Article Content --}}
                              <div class="article-content">
                                  <div class="wrapper">


                                      {!! $article->content !!}

                                  </div>
                              </div>

                              {{-- Article Author --}}
                              <footer class="article-footer">
                                  <div class="article-details" style="padding-top: 15px;padding-bottom: 15px;">
                                    <div class="row">



                                            @foreach ($article->tags as $tag)
                                              <div class="col-md-3" style="margin-bottom:10px;">
                                                <div id="tags" >

                                                    {{$tag->name}}
                                                </div>
                                                </div>
                                            @endforeach



                                  </div>
                                <div class="row">
                                        <div class="col-md-12">
                                          <div class="media media-article-title share-icons" style="margin-bottom:0px;">
                                            <a href="#" class="fa fa-facebook sharer" data-sharer="facebook" data-title="{{$article->title}}" data-url="{{route('showArticle',htmlentities(urlencode(str_replace(' ', '-', $article->title))).'_'.$article->id)}}"></a>
                                            <a href="#" class="fa fa-twitter sharer" data-sharer="twitter"  data-title="{{$article->title}}" data-url="{{route('showArticle',htmlentities(urlencode(str_replace(' ', '-', $article->title))).'_'.$article->id)}}"></a>
                                            <a href="#" class="fa fa-linkedin sharer" data-sharer="linkedin"  data-url="{{route('showArticle',htmlentities(urlencode(str_replace(' ', '-', $article->title))).'_'.$article->id)}}"></a>
                                            <a href="#" class="fa fa-pinterest  sharer" data-sharer="pinterest" data-url="{{route('showArticle',htmlentities(urlencode(str_replace(' ', '-', $article->title))).'_'.$article->id)}}" data-title="{{$article->title}}" data-image="{{asset($article->image)}}" data-description="{{$article->description}}"></a>

                                          </div>

                                          {{-- <div class="sharethis-inline-share-buttons"></div> --}}
                                        </div>

                                    </div>
                                  </div>
                              </footer>

                              {{-- Comment Content --}}
                              <footer class="article-footer">
                                  <div class="row bootstrap snippets">
                                      <div class="offset-md-1 col-md-10 col-sm-10">
                                          <div class="comment-wrapper">
                                              <div class="panel panel-info">
                                                  <div class="panel-heading">
                                                      Comment panel
                                                  </div>
                                                  <div class="panel-body" style="position:relative;">
                                                      <textarea class="form-control" placeholder="write a comment..." rows="3" id="content-comment"></textarea>
                                                      <input type="hidden" id = "articleId" name="articleId" value="{{$article->id}}">
                                                      <br>
                                                      <button type="button" id = "post-comment" class="btn btn-info pull-right">Post</button>
                                                      <div class="clearfix"></div>
                                                      <br>
                                                    <div id="article-comments">
                                                        @foreach ($article->comments()->skip(0)->take(3)->get() as $comment)
                                                        <div class="media comments">
                                                            <img class="d-flex rounded-circle avatar z-depth-1-half mr-3" src="{{asset($comment->author->image)}}" alt="Avatar">
                                                            <div class="media-body">
                                                                <h5 class="mt-0 font-weight-bold blue-text">{{$comment->author->first_name}}</h5>
                                                                <span>{{$comment->content}}</span>

                                                                  <a href="#" class="show-more-replay" data-replies='{{$comment->id}}'>

                                                                    @if (count($comment->comments) > 0)
                                                                        Show replies <span>({{count($comment->comments)}})</span>

                                                                      @else

                                                                      <span class="no-replies">No replies</span>

                                                                    @endif


                                                                  </a>
                                                            </div>

                                                            <br>
                                                            <a href="#" class="respense" data-comment='{{$comment->id}}'><i class="fa fa-reply"></i></a>


                                                        </div>

                                                        <div id="comment-replay{{$comment->id}}">


                                                        </div>

                                                        <div id="comment-respense{{$comment->id}}">

                                                        </div>
                                                        @endforeach

                                                    </div>
                                              </div>
                                                @if (count($article->comments) > 3 )
                                                  <a href="#" id="show-more-comment" data-offset = "1">Show More1</a>
                                                @endif

                                          </div>

                                      </div>
                                  </div>
                              </footer>
                          </article>

                          <div class="btns-action row item-buttons">


                            <div class="row">
                              <div class="col-xs-6">
                                <a class="button btn-white text-center" href="{{route('showGallery')}}" style="border:none;">
                                    <span class="txt"><i class="fa fa-long-arrow-left mr-2"></i> Back To Blog</span>
                                </a>
                              </div>
                                <div class="col-xs-6  item-buttons-next">


                                  @if ($previous)
                                    <a class="button btn-white" href="{{route('showArticle',str_replace(' ', '-', $previousArticle->title).'_'.$previous)}}">
                                        <i class="icon fa fa-long-arrow-left mr-2"></i>
                                        <span class="txt">Prev</span>
                                    </a>
                                  @endif

                                  @if ($next)
                                    <a class=" button btn-white" href="{{route('showArticle',str_replace(' ', '-', $nextArticle->title).'_'.$next)}}">
                                        <span class="txt">Next</span>
                                        <i class="icon fa fa-long-arrow-right ml-2"></i>
                                    </a>
                                  @endif


                                </div>
                            </div>


                          </div>
                      </div>

                  </div>

              </div>

          </div>


        </div>
    </div>
      {{-- <a href="#" data-toggle="modal" data-target="#loginModal">Modal login</a> --}}


</main>



@include('General::modals.login')
@endsection
@section('js')
@include('General::inc.detailsScript')
<script src="{{asset('dipper/js/sharer.js')}}" charset="utf-8"></script>

@endsection
