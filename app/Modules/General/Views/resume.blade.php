<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
    <meta name="description" content="QUOTE - Request a quote for every type of companies">
    <meta name="author" content="Ansonika">
    <title>QUOTE - Request a quote for every type of companies</title>

    <!-- Favicons-->
    <link rel="shortcut icon" href="{{asset('brief/img/favicon.ico')}}" type="image/x-icon">
    <link rel="apple-touch-icon" type="image/x-icon" href="img/apple-touch-icon-57x57-precomposed.png">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="72x72" href="img/apple-touch-icon-72x72-precomposed.png">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="114x114" href="img/apple-touch-icon-114x114-precomposed.png">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="144x144" href="img/apple-touch-icon-144x144-precomposed.png">

    <!-- GOOGLE WEB FONT -->
    <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:300,300i,400,400i,700,700i" rel="stylesheet">

    <!-- BASE CSS -->
    <link href="{{asset('brief/css/animate.min.css')}}" rel="stylesheet">
    <link href="{{asset('brief/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('brief/css/style.css')}}" rel="stylesheet">
    <link href="{{asset('brief/css/icon_fonts/css/all_icons_min.css')}}" rel="stylesheet">
    <link href="{{asset('brief/css/magnific-popup.min.css')}}" rel="stylesheet">
    <link href="{{asset('brief/css/skins/square/yellow.css')}}" rel="stylesheet">

    <!-- YOUR CUSTOM CSS -->
    <link href="{{asset('brief/css/custom.css')}}" rel="stylesheet">

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/css/bootstrap-select.min.css">
    <style>
        .accordion {
            background-color: #ffcc7b;
            color: #444;
            cursor: pointer;
            padding: 18px;
            width: 100%;
            border: none;
            text-align: left;
            outline: none;
            font-size: 15px;
            transition: 0.4s;
        }

        .active,
        .accordion:hover {
            /* background-color: #ccc; */
        }

        .panel {
            padding: 18px 18px;
            display: none;
            background-color: white;
            overflow: hidden;
        }
    </style>


</head>

<style>
    #other_accounts,
    #other_types,
    #other_site_type,
    #other_pages,
    #other_languages {
        display: none;
    }
</style>

<body>

    <div id="loader_form">
        <div data-loader="circle-side-2"></div>
    </div><!-- /Loader_form -->

    {{-- <header>
        <a id="menu-button-mobile" class="cmn-toggle-switch cmn-toggle-switch__htx" href="javascript:void(0);"><span>Menu mobile</span></a>
        <nav class="main_nav">
            <ul class="nav nav-tabs">
                <li><a href="#tab_1" data-toggle="tab" class="active show">Request a quote</a></li>
                <li><a href="#tab_2" data-toggle="tab">About</a></li>
                <li><a href="#tab_3" data-toggle="tab">Contact</a></li>
            </ul>
        </nav>
    </header><!-- /header --> --}}

    <div id="main_container" class="visible">

        {{-- <div id="header_in">
            <div id="logo_in"><img src="{{asset('assets/logoDipper/logo3.png')}}" width="160" height="48" data-retina="true" alt="Quote"></div>
    </div> --}}

    <div class="wrapper_in">
        <div class="container-fluid">
            <div class="tab-content">
                <div class="tab-pane fade show active" id="tab_1">
                    <div class="subheader" id="quote"></div>
                    <div class="row">
                        <aside class="col-xl-3 col-lg-4">
                            <h2>Request a Quote and Compare prices!</h2>
                            <p class="lead">An mei sadipscing dissentiet, eos ea partem viderer facilisi.</p>
                            <ul class="list_ok">
                                <li>Delicata persecuti ei nec, et his minim omnium, aperiam placerat ea vis.</li>
                                <li>Suavitate vituperatoribus pro ad, cum in quis propriae abhorreant.</li>
                                <li>Aperiri deterruisset ei mea, sed cu laudem intellegat, eu mutat iuvaret voluptatum mei.</li>
                            </ul>
                        </aside><!-- /aside -->

                        <div class="col-xl-9 col-lg-8">
                            <div id="wizard_container">
                              <button class="accordion"  style="background:#FE8457">General Information</button>
                              <div class="panel">

                                <div class="col-xs-12 col-sm-12">
                                     <h2>{{$business->nom}}</h2>
                                     <p><strong>Address: </strong> {{$business->address}} </p>
                                     <p><strong>Email: </strong> {{$business->email}} </p>
                                     <p><strong>Phone: </strong> {{$business->phone}} </p>
                                     <p><strong>Fax: </strong> {{$business->fax}} </p>
                                     <p><strong>Activity: </strong> {{$business->activity}} </p>

                                 </div>

                              </div>
                                <button class="accordion">Contact Person</button>
                                <div class="panel">
                                  <div class="col-xs-12 col-sm-12">
                                       <h2>{{$business->personal->nom}}</h2>
                                       <p><strong>Position: </strong> {{$business->personal->position}} </p>
                                       <p><strong>Email: </strong> {{$business->personal->email}} </p>
                                       <p><strong>Phone: </strong> {{$business->personal->phone}} </p>
                                   </div>

                                </div>

                                <button class="accordion" style="background:#FE8457">Company portfolio</button>
                                <div class="panel">
                                  <div class="col-xs-12 col-sm-12">

                                       <p><strong>Votre service : </strong> {{$business->portfolio->service}} </p>
                                       <p><strong>Votre avantage concurrentiel : </strong> {{$business->portfolio->va}} </p>
                                       <p><strong>Les principes/valeurs : </strong> {{$business->portfolio->values}} </p>
                                       <p><strong>Vos principaux concurrents : </strong> {{$business->portfolio->concurrents}} </p>
                                       <p><strong>Votre cible : </strong> {{$business->portfolio->audience}} </p>
                                       <p><strong>Vos Marchés Visés : </strong> {{$business->portfolio->market}} </p>
                                   </div>
                                </div>



                                <button class="accordion" >Community Management</button>
                                <div class="panel">
                                  <div class="col-xs-12 col-sm-12">
                                       <p><strong>Votre réseaux sociaux: </strong> {{$business->management->existent_accounts ? implode(",", unserialize($business->management->existent_accounts)) : ""}} </p>
                                       <p><strong>Les réseaux sociaux a crées: </strong> {{$business->management->needed_accounts ? implode(",", unserialize($business->management->needed_accounts)) : ""}}</p>
                                       <p><strong>Nombres des publications par semaines: </strong> {{$business->management->max_posts}}</p>
                                       <p><strong>Les types de publication : </strong>  {{$business->management->post_types ? implode(",", unserialize($business->management->post_types)) : ""}}</p>
                                       <p><strong>Les réseaux socials que vous voulez les publier  : </strong> {{$business->management->preferred_supports ? implode(",", unserialize($business->management->preferred_supports)) : ""}}</p>
                                       <p><strong>Sponsoriser ces publications : </strong> {{$business->management->sponsored}} </p>
                                       <p><strong>sponsoring : </strong> {{$business->management->sponsored_supports ? implode(",", unserialize($business->management->sponsored_supports)) : ""}}</p>
                                       <p><strong>Objectif du sponsoring : </strong> {{$business->management->sponsor_goal}} </p>
                                       <p><strong>Durée du contrat de la gestion de vos réseaux sociaux : </strong> {{$business->management->contract_delay}} </p>
                                       <p><strong>Rapport de l'etat de l'évolution de votre présence sur les réseaux sociaux : </strong> {{$business->management->reporting_delay}} </p>
                                   </div>
                                </div>

                                  @if ($business->shooting)
                                    <button class="accordion"  style="background:#FE8457">Shooting</button>
                                    <div class="panel">
                                      <div class="col-xs-12 col-sm-12">


                                           <p><strong>Shooting Type: </strong> {{$business->shooting->type ? implode(",", unserialize($business->shooting->type)) : ""}}</p>
                                           <p><strong>Type des photos prises: </strong>{{$business->shooting->photo_types ? implode(",", unserialize($business->shooting->photo_types)) : ""}}</p>
                                           <p><strong>Catégorie des photos à prendre: </strong> {{$business->shooting->categories ? implode(",", unserialize($business->shooting->categories)) : ""}}</p>
                                           <p><strong>Nombres des photos : </strong> {{$business->shooting->number}} </p>
                                           <p><strong>Thème de la vidéo à concevoir  : </strong> {{$business->shooting->theme}} </p>
                                           <p><strong>Longueur de la vidéo à concevoir : </strong> {{$business->shooting->length}} </p>

                                       </div>
                                    </div>
                                  @endif

                                <button class="accordion" >Website Developement</button>
                                <div class="panel">
                                  <div class="col-xs-12 col-sm-12">

                                       <p><strong>Avez-vous un site web: </strong> {{$business->development->have}} </p>
                                       <p><strong>Creation d'un neveau site: </strong> {{$business->development->create}} </p>
                                       <p><strong>Modérer votre site web: </strong> {{$business->development->moderate}} </p>
                                       <p><strong>L’adresse du votre site web : </strong> {{$business->development->url}} </p>
                                       <p><strong>Technique de développement utilisée   : </strong> {{$business->development->techs}} </p>
                                       <p><strong>Type de votre site web : </strong> {{$business->development->type ? implode(",", unserialize($business->development->type)) : ""}}</p>
                                       <p><strong>Autre Type : </strong> {{$business->development->other_site_type}} </p>
                                       <p><strong>Avez-vous un nom de domaine  : </strong> {{$business->development->domain}} </p>
                                       <p><strong>Avez-vous un hébergement   : </strong> {{$business->development->hosting}} </p>
                                       <p><strong>Le nom de votre domaine  : </strong> {{$business->development->domain}} </p>
                                       <p><strong>Le nom de votre hébergement   : </strong> {{$business->development->hosting}} </p>
                                       <p><strong>Les fonctionnalités de votre site web    : </strong> {{$business->development->pages ? implode(",", unserialize($business->development->pages)) : ""}} </p>
                                       <p><strong>Les languages de votre site web    : </strong> {{$business->development->languages ? implode(",", unserialize($business->development->languages)) : ""}} </p>
                                       <p><strong>Le contenu  : </strong> {{$business->development->content}} </p>
                                       <p><strong>Type contenu : </strong> {{$business->development->content_type ? implode(",", unserialize($business->development->content_type)) : ""}}</p>
                                       <p><strong>Theme de votre site : </strong> {{$business->development->theme ? implode(",", unserialize($business->development->theme)) : ""}}</p>

                                   </div>
                                </div>

                                <button class="accordion"  style="background:#FE8457">Graphic Design</button>
                                <div class="panel">
                                  <p><strong>Logo: </strong> {{$business->graphic->logo}}</p>
                                  <p><strong>Cartes de visites: </strong> {{$business->graphic->cards}}</p>
                                  <p><strong>Flyers : </strong> {{$business->graphic->flyers}}</p>
                                  <p><strong>Catalogues : </strong> {{$business->graphic->catalogue}}</p>
                                  <p><strong>Charte graphique : </strong> {{$business->graphic->chart}}</p>
                                  <p><strong>Présentation PPT: </strong> {{$business->graphic->ppt}}</p>
                                  <p><strong>Affiche publicitaire: </strong> {{$business->graphic->affiche}}</p>
                                </div>

                            </div><!-- /Wizard container -->

                        </div><!-- /col -->
                    </div><!-- /row -->
                </div><!-- /TAB 1:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: -->





            </div><!-- /tab content -->
        </div><!-- /container-fluid -->
    </div><!-- /wrapper_in -->
    </div><!-- /main_container -->




    <!-- Jquery-->
    <script src="{{asset('brief/js/jquery-3.2.1.min.js')}}"></script>
    <!-- Common script -->
    <script src="{{asset('brief/js/common_scripts.js')}}"></script>
    <!-- Theme script -->
    <script src="{{asset('brief/js/functions_no_side_panel.js')}}"></script>





    <!-- Latest compiled and minified JavaScript -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/js/bootstrap-select.min.js"></script>



    <script>
    var acc = document.getElementsByClassName("accordion");
    var i;

    for (i = 0; i < acc.length; i++) {
      acc[i].addEventListener("click", function() {
        this.classList.toggle("active");
        var panel = this.nextElementSibling;
        if (panel.style.display === "block") {
          panel.style.display = "none";
        } else {
          panel.style.display = "block";
        }
      });
    }
    </script>


</body>

</html>
