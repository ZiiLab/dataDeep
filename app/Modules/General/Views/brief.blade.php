<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
    <meta name="description" content="QUOTE - Request a quote for every type of companies">
    <meta name="author" content="Ansonika">
    <title>QUOTE - Request a quote for every type of companies</title>

    <!-- Favicons-->
    <link rel="shortcut icon" href="{{asset('brief/img/favicon.ico')}}" type="image/x-icon">
    <link rel="apple-touch-icon" type="image/x-icon" href="img/apple-touch-icon-57x57-precomposed.png">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="72x72" href="img/apple-touch-icon-72x72-precomposed.png">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="114x114" href="img/apple-touch-icon-114x114-precomposed.png">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="144x144" href="img/apple-touch-icon-144x144-precomposed.png">

    <!-- GOOGLE WEB FONT -->
    <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:300,300i,400,400i,700,700i" rel="stylesheet">

    <!-- BASE CSS -->
    <link href="{{asset('brief/css/animate.min.css')}}" rel="stylesheet">
    <link href="{{asset('brief/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('brief/css/style.css')}}" rel="stylesheet">
    <link href="{{asset('brief/css/icon_fonts/css/all_icons_min.css')}}" rel="stylesheet">
    <link href="{{asset('brief/css/magnific-popup.min.css')}}" rel="stylesheet">
    <link href="{{asset('brief/css/skins/square/yellow.css')}}" rel="stylesheet">

    <!-- YOUR CUSTOM CSS -->
    <link href="{{asset('brief/css/custom.css')}}" rel="stylesheet">

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/css/bootstrap-select.min.css">



</head>

<style>


#other_accounts, #other_types, #other_site_type,#other_pages,#other_languages{
  display:none;
}

</style>
<body>

    <div id="loader_form">
        <div data-loader="circle-side-2"></div>
    </div><!-- /Loader_form -->

    {{-- <header>
        <a id="menu-button-mobile" class="cmn-toggle-switch cmn-toggle-switch__htx" href="javascript:void(0);"><span>Menu mobile</span></a>
        <nav class="main_nav">
            <ul class="nav nav-tabs">
                <li><a href="#tab_1" data-toggle="tab" class="active show">Request a quote</a></li>
                <li><a href="#tab_2" data-toggle="tab">About</a></li>
                <li><a href="#tab_3" data-toggle="tab">Contact</a></li>
            </ul>
        </nav>
    </header><!-- /header --> --}}

    <div id="main_container" class="visible">

        {{-- <div id="header_in">
            <div id="logo_in"><img src="{{asset('assets/logoDipper/logo3.png')}}" width="160" height="48" data-retina="true" alt="Quote"></div>
    </div> --}}

    <div class="wrapper_in">
        <div class="container-fluid">
            <div class="tab-content">
                <div class="tab-pane fade show active" id="tab_1">
                    <div class="subheader" id="quote"></div>
                    <div class="row">
                        <aside class="col-xl-3 col-lg-4">
                            <h2>Request a Quote and Compare prices!</h2>
                            <p class="lead">An mei sadipscing dissentiet, eos ea partem viderer facilisi.</p>
                            <ul class="list_ok">
                                <li>Delicata persecuti ei nec, et his minim omnium, aperiam placerat ea vis.</li>
                                <li>Suavitate vituperatoribus pro ad, cum in quis propriae abhorreant.</li>
                                <li>Aperiri deterruisset ei mea, sed cu laudem intellegat, eu mutat iuvaret voluptatum mei.</li>
                            </ul>
                        </aside><!-- /aside -->

                        <div class="col-xl-9 col-lg-8">
                            <div id="wizard_container">
                                <div id="top-wizard">
                                    <strong>Progress</strong>
                                    <div id="progressbar"></div>
                                </div><!-- /top-wizard -->

                                <form name="example-1" id="wrapped" method="POST">

                                  {{ csrf_field() }}
                                    <input id="website" name="website" type="text" value=""><!-- Leave for security protection, read docs for details -->
                                    <input type="hidden" id="business_id" >
                                    <div id="middle-wizard">

                                        {{-- Information General --}}
                                        <div class="step">

                                            <h3 class="main_question"><strong>1/7</strong>General Information</h3>

                                            <div class="row">

                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <input type="text" name="nom" class="form-control" id="nom" placeholder="Nom de la Société ">
                                                    </div>
                                                    <div class="form-group">
                                                        <input type="text" name="address" id="address" class="form-control" placeholder="Address">
                                                    </div>
                                                    <div class="form-group">
                                                        <input type="email" name="email" id="email" class="form-control" placeholder="Email">
                                                    </div>

                                                </div><!-- /col-sm-6 -->

                                                <div class="col-sm-6">

                                                    <div class="form-group">
                                                        <input type="text" name="phone" id="phone" class="form-control" placeholder="Votre Téléphone">
                                                    </div>
                                                    <div class="form-group">
                                                        <input type="text" name="fax" id="fax" class="form-control" placeholder="Votre Fax">
                                                    </div>
                                                    <div class="form-group">
                                                        <input type="text" name="activity" id="activity" class="form-control" placeholder="Votre Activité">
                                                    </div>
                                                </div><!-- /col-sm-6 -->
                                            </div><!-- /row -->

                                        </div><!-- General Information-->
                                        {{-- End Information General --}}

                                        {{-- Contact Person --}}
                                        <div class="step">
                                            <h3 class="main_question"><strong>2/7</strong>Contact Person</h3>

                                            <div class="form-group">
                                                <input type="text" name="contact-nom" id="contact-nom" class="form-control" placeholder="Nom Personne à Contacter">
                                            </div>
                                            <div class="form-group">
                                                <input type="text" name="contact-position" id="contact-position" class="form-control" placeholder="Position">
                                            </div>

                                            <div class="form-group">
                                                <input type="email" name="contact-email" id="contact-email" class="form-control" placeholder="Email">
                                            </div>

                                            <div class="form-group">
                                                <input type="text" name="contact-phone" id="contact-phone" class="form-control" placeholder="Téléphone">
                                            </div>



                                        </div><!-- /step 1-->
                                        {{-- end Contact Person --}}

                                        {{-- Company portfolio --}}
                                        <div class="step">
                                            <h3 class="main_question"><strong>3/7</strong>Company portfolio</h3>

                                            <div class="form-group textarea_info">
                                                <label>1. Quels produits/services Offrez-vous </label>
                                                <textarea name="service" id="service" class="form-control" style="height:75px;" placeholder="Produits / Services"></textarea>
                                            </div>

                                            <div class="form-group textarea_info">
                                                <label>2. Quel est votre avantage concurrentiel (VA par rapport à vos concurrents) </label>
                                                <textarea name="va" id="va" class="form-control" style="height:75px;" placeholder="Valeur Ajouté"></textarea>
                                            </div>

                                            <div class="form-group textarea_info">
                                                <label>3. Quels sont les principes/valeurs de votre société ? </label>
                                                <textarea name="values" id="values" class="form-control" style="height:75px;" placeholder="Principes / Valeurs"></textarea>
                                            </div>

                                            <div class="form-group textarea_info">
                                                <label>4. Quels sont vos principaux concurrents ? </label>
                                                <textarea name="concurrent" id="concurrents" class="form-control" style="height:75px;" placeholder="Principaux Concurrents"></textarea>
                                            </div>

                                            <div class="form-group textarea_info">
                                                <label>5. Quel est votre cible ? </label>
                                                <textarea name="audience" class="form-control" style="height:75px;" placeholder="Votre Cible"></textarea>
                                            </div>

                                            <div class="form-group select">
                                                <label>6. Quels Sont Vos Marchés Visés : </label>
                                                <div class="styled-select">
                                                    <select class=" " name="market" id="market">
                                                        <option value="national" selected>National</option>
                                                        <option value="international">International</option>
                                                        <option value="local">Local</option>
                                                        <option value="virtual">Vertuel</option>
                                                    </select>
                                                </div>
                                            </div><!-- /select-->
                                        </div><!-- /step 2-->
                                        {{--End Company portfolio --}}


                                        {{-- Community Management  --}}
                                        <div class="step">
                                            <h3 class="main_question"><strong>4/7</strong>Community Management</h3>

                                            <h6 class="main_question" style="font-weight: 400; color: #222;">1. Disposez-vous un compte/page sur les réseaux sociaux suivants : </h6>
                                            <div class="row add_bottom_30">

                                                <div class="col-sm-6">
                                                    <div class="form-group checkbox_questions">
                                                        <label>
                                                            <input name="existent_accounts[]" type="checkbox" value="facebook" class="existent_accounts icheck  ">Facebook
                                                        </label>
                                                    </div>
                                                    <div class="form-group checkbox_questions">
                                                        <label>
                                                            <input name="existent_accounts[]" type="checkbox" value="linkedin" class="existent_accounts icheck  ">Linkedin
                                                        </label>
                                                    </div>
                                                    <div class="form-group checkbox_questions">
                                                        <label>
                                                            <input name="existent_accounts[]" type="checkbox" value="instagram" class="existent_accounts icheck  ">Instagram
                                                        </label>
                                                    </div>
                                                </div>

                                                <div class="col-sm-6">
                                                    <div class="form-group checkbox_questions">
                                                        <label>
                                                            <input name="existent_accounts[]" type="checkbox" value="youtube" class="existent_accounts icheck  ">Youtube
                                                        </label>
                                                    </div>
                                                    <div class="form-group checkbox_questions">
                                                        <label>
                                                            <input name="existent_accounts[]" type="checkbox" value="twitter" class="existent_accounts icheck  ">Twitter
                                                        </label>
                                                    </div>


                                                </div>
                                            </div><!-- /row-->



                                            <h6 class="main_question" style="font-weight: 400; color: #222;">2. Voulez vous créer un compte/page sur les réseaux sociaux suivants : </h6>
                                            <div class="row add_bottom_30">

                                                <div class="col-sm-6">
                                                    <div class="form-group checkbox_questions">
                                                        <label>
                                                            <input name="needed_accounts[]" type="checkbox" value="facebook" class="needed_accounts icheck  ">Facebook
                                                        </label>
                                                    </div>
                                                    <div class="form-group checkbox_questions">
                                                        <label>
                                                            <input name="needed_accounts[]" type="checkbox" value="linkedin" class="needed_accounts icheck  ">Linkedin
                                                        </label>
                                                    </div>
                                                    <div class="form-group checkbox_questions">
                                                        <label>
                                                            <input name="needed_accounts[]" type="checkbox" value="instagram" class="needed_accounts icheck  ">Instagram
                                                        </label>
                                                    </div>
                                                </div>

                                                <div class="col-sm-6">
                                                    <div class="form-group checkbox_questions">
                                                        <label>
                                                            <input name="needed_accounts[]" type="checkbox" value="youtube" class="needed_accounts icheck  ">Youtube
                                                        </label>
                                                    </div>
                                                    <div class="form-group checkbox_questions">
                                                        <label>
                                                            <input name="needed_accounts[]" type="checkbox" value="twitter" class="needed_accounts icheck  ">Twitter
                                                        </label>
                                                    </div>

                                                </div>
                                            </div><!-- /row-->

                                            <div class="form-group textarea_info">
                                                <label>3. Combien de publications par semaine ? </label>
                                                <input type="number" name="max_posts" class="form-control" id="max_posts" placeholder="Nombre de publications min:1 max:4" min="1" max="4">
                                            </div>

                                            <label>4. Quel est le type de  publication  ? :</label>
                                            <div class="">
                                                <select class="  selectpicker" name="post_types[]" id="post_types" multiple style="width:100%">

                                                    <option value="Contenu visuel (Image)">Contenu visuel (Image)</option>
                                                    <option value="Contenu audio visuelle (Vidéo)">Contenu audio visuelle (Vidéo)</option>
                                                    <option value="Citation du jour">Citation du jour</option>

                                                    <option value="Conseils qui concernent votre DAS">Conseils qui concernent votre DAS</option>
                                                    <option value="Portraits de vos collaborateurs">Portraits de vos collaborateurs</option>
                                                    <option value="Articles">Articles</option>

                                                    <option value="Témoignages de vos clients/visiteurs">Témoignages de vos clients/visiteurs</option>
                                                    <option value="Invitation événementielle">Invitation événementielle</option>
                                                    <option value="Annonce de promotion">Annonce de promotion</option>

                                                    <option value="Coulisses">Coulisses</option>
                                                    <option value="Vidéo live">Vidéo live</option>
                                                    <option value="Story">Story</option>

                                                    <option value="" id="types">Autre</option>


                                                </select>
                                            </div>
                                            <div class="form-group textarea_info">
                                                <textarea name="other_types" class="form-control" id="other_types" style="height:150px;" placeholder=""></textarea>
                                            </div>

                                            <h6 class="main_question" style="font-weight: 400; color: #222;">5. Dans quel réseau social voulez vous les publier ?</h6>
                                            <div class="row add_bottom_30">

                                                <div class="col-sm-6">
                                                    <div class="form-group checkbox_questions">
                                                        <label>
                                                            <input name="preferred_supports[]" type="checkbox" value="facebook" class="preferred_supports icheck  ">Facebook
                                                        </label>
                                                    </div>
                                                    <div class="form-group checkbox_questions">
                                                        <label>
                                                            <input name="preferred_supports[]" type="checkbox" value="linkedin" class="preferred_supports icheck  ">Linkedin
                                                        </label>
                                                    </div>
                                                    <div class="form-group checkbox_questions">
                                                        <label>
                                                            <input name="preferred_supports[]" type="checkbox" value="instagram" class="preferred_supports icheck  ">Instagram
                                                        </label>
                                                    </div>
                                                </div>

                                                <div class="col-sm-6">
                                                    <div class="form-group checkbox_questions">
                                                        <label>
                                                            <input name="preferred_supports[]" type="checkbox" value="youtube" class="preferred_supports icheck  ">Youtube
                                                        </label>
                                                    </div>
                                                    <div class="form-group checkbox_questions">
                                                        <label>
                                                            <input name="preferred_supports[]" type="checkbox" value="twitter" class="preferred_supports icheck  ">Twitter
                                                        </label>
                                                    </div>

                                                </div>
                                            </div><!-- /row-->



                                            <h6 class="main_question" style="font-weight: 400; color: #222;">6. Voulez vous sponsoriser ces publications ?</h6>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group radio_questions">
                                                        <label>1. Oui
                                                            <input name="sponsored" type="radio" value="1" class="icheck sponsored">
                                                        </label>
                                                    </div>
                                                </div>

                                                <div class="col-md-4">
                                                    <div class="form-group radio_questions">
                                                        <label>2.Non
                                                            <input name="sponsored" type="radio" value="0" class="icheck sponsored">
                                                        </label>
                                                    </div>
                                                </div>

                                                <div class="col-md-4">
                                                    <div class="form-group radio_questions">
                                                        <label>3.Peut-être
                                                            <input name="sponsored" type="radio" value="2" class="icheck sponsored">
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>


                                            <h6 class="main_question" style="font-weight: 400; color: #222;">7. Quel réseau social vous visez pour faire du sponsoring ?</h6>
                                            <div class="row add_bottom_30">

                                                <div class="col-sm-6">
                                                    <div class="form-group checkbox_questions">
                                                        <label>
                                                            <input name="sponsor_vision[]" type="checkbox" value="facebook" class="sponsor_vision icheck  ">Facebook
                                                        </label>
                                                    </div>
                                                    <div class="form-group checkbox_questions">
                                                        <label>
                                                            <input name="sponsor_vision[]" type="checkbox" value="linkedin" class="sponsor_vision icheck  ">Linkedin
                                                        </label>
                                                    </div>
                                                    <div class="form-group checkbox_questions">
                                                        <label>
                                                            <input name="sponsor_vision[]" type="checkbox" value="instagram" class="sponsor_vision icheck  ">Instagram
                                                        </label>
                                                    </div>
                                                </div>

                                                <div class="col-sm-6">
                                                    <div class="form-group checkbox_questions">
                                                        <label>
                                                            <input name="sponsor_vision[]" type="checkbox" value="youtube" class="sponsor_vision icheck  ">Youtube
                                                        </label>
                                                    </div>
                                                    <div class="form-group checkbox_questions">
                                                        <label>
                                                            <input name="sponsor_vision[]" type="checkbox" value="twitter" class="sponsor_vision icheck  ">Twitter
                                                        </label>
                                                    </div>


                                                </div>
                                            </div><!-- /row-->

                                            <h6 class="main_question" style="font-weight: 400; color: #222;">8. Quel est votre objectif du sponsoring ?</h6>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group radio_questions">
                                                        <label>1. Promouvoir
                                                            <input name="sponsor_goal" type="radio" value="promouvoir" class="icheck sponsor_goal">
                                                        </label>
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group radio_questions">
                                                        <label>2.Booster
                                                            <input name="sponsor_goal" type="radio" value="booster" class="icheck sponsor_goal">
                                                        </label>
                                                    </div>
                                                </div>


                                            </div>


                                            <h6 class="main_question" style="font-weight: 400; color: #222;">9. Quelle est la durée du contrat de la gestion de vos réseaux sociaux ?</h6>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group radio_questions">
                                                        <label>1. 3 mois
                                                            <input name="contract_delay" type="radio" value="3mois" class="icheck contract_delay">
                                                        </label>
                                                    </div>
                                                </div>

                                                <div class="col-md-4">
                                                    <div class="form-group radio_questions">
                                                        <label>2. 6 mois
                                                            <input name="contract_delay" type="radio" value="6mois" class="icheck contract_delay">
                                                        </label>
                                                    </div>
                                                </div>

                                                <div class="col-md-4">
                                                    <div class="form-group radio_questions">
                                                        <label>3. 12 mois
                                                            <input name="contract_delay" type="radio" value="12mois" class="icheck contract_delay">
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>

                                            <h6 class="main_question" style="font-weight: 400; color: #222;">10. Souhaitez-vous recevoir un reporting de l’état de l’évolution de votre présence sur les réseaux sociaux ?</h6>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group radio_questions">
                                                        <label>1. Hebdomadaire
                                                            <input name="reporting_delay" type="radio" value="hebdomadaire" class="icheck reporting_delay">
                                                        </label>
                                                    </div>
                                                </div>

                                                <div class="col-md-4">
                                                    <div class="form-group radio_questions">
                                                        <label>2. Mensuelle
                                                            <input name="reporting_delay" type="radio" value="mensuelle" class="icheck reporting_delay">
                                                        </label>
                                                    </div>
                                                </div>

                                                <div class="col-md-4">
                                                    <div class="form-group radio_questions">
                                                        <label>3. Trimestrielle
                                                            <input name="reporting_delay" type="radio" value="trimestrielle" class="icheck reporting_delay">
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>

                                        </div><!-- /step 2-->
                                        {{--End Community Management  --}}

                                        {{--Shooting --}}
                                        <div class="step">
                                            <h3 class="main_question"><strong>5/7</strong>Shooting</h3>

                                            <h6 class="main_question" style="font-weight: 400; color: #222;">1. Voulez vous faire un shooting ?</h6>

                                            <div class="row add_bottom_30">

                                                <div class="col-sm-4">
                                                    <div class="form-group checkbox_questions">
                                                        <label>
                                                            <input name="typeImg[]" type="checkbox" value="Shooting photos" class="type icheck  ">1. Shooting photos
                                                        </label>
                                                    </div>

                                                </div>

                                                <div class="col-sm-4">
                                                    <div class="form-group checkbox_questions">
                                                        <label>
                                                            <input name="type[]" type="checkbox" value="Shooting vidéos" class="type icheck  ">2. Shooting vidéos
                                                        </label>
                                                    </div>

                                                </div>

                                                <div class="col-sm-4">
                                                    <div class="form-group checkbox_questions">
                                                        <label>
                                                            <input name="type[]" type="checkbox" value="Non" class="type icheck  ">3. Non
                                                        </label>
                                                    </div>

                                                </div>
                                            </div><!-- /row-->


                                            <h6 class="main_question" style="font-weight: 400; color: #222;">2. Quel est le type des photos prises ?</h6>
                                            <div class="row add_bottom_30">

                                                <div class="col-sm-6">
                                                    <div class="form-group checkbox_questions">
                                                        <label>
                                                            <input name="photo_types[]" type="checkbox" value="Photos de haute résolution" class="photo_types icheck  ">Photos de haute résolution
                                                        </label>
                                                    </div>

                                                </div>

                                                <div class="col-sm-6">
                                                    <div class="form-group checkbox_questions">
                                                        <label>
                                                            <input name="photo_types[]" type="checkbox" value="Photos 360" class="photo_types icheck  ">Photos 360
                                                        </label>
                                                    </div>

                                                </div>


                                            </div><!-- /row-->


                                            <h6 class="main_question" style="font-weight: 400; color: #222;">3. Quelle est la catégorie des photos à prendre ?</h6>
                                            <div class="row add_bottom_30">

                                                <div class="col-sm-4">
                                                    <div class="form-group checkbox_questions">
                                                        <label>
                                                            <input name="categories[]" type="checkbox" value="Lieu" class="categories icheck  ">Lieu
                                                        </label>
                                                    </div>

                                                </div>

                                                <div class="col-sm-4">
                                                    <div class="form-group checkbox_questions">
                                                        <label>
                                                            <input name="categories[]" type="checkbox" value="Portrait" class="categories icheck  ">Portrait
                                                        </label>
                                                    </div>

                                                </div>

                                                <div class="col-sm-4">
                                                    <div class="form-group checkbox_questions">
                                                        <label>
                                                            <input name="categories[]" type="checkbox" value="Produits" class="categories icheck  ">Produits
                                                        </label>
                                                    </div>

                                                </div>


                                            </div><!-- /row-->

                                            <div class="form-group textarea_info">
                                                <label>4. Combien de photos voulez vous prendre ? </label>
                                                <input type="number" name="number" class="form-control" id="number" placeholder="Nombre de photos">
                                            </div>
                                            <div class="form-group textarea_info">
                                                <label>5. Quel est le thème de la vidéo à concevoir ? </label>
                                                <input type="text" name="theme" class="form-control" id="theme" placeholder="Thème de video">
                                            </div>
                                            <h6 class="main_question" style="font-weight: 400; color: #222;">6. Quel est la longueur de la vidéo à concevoir ? </h6>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group radio_questions">
                                                        <label>1. Moins 30secondes
                                                            <input name="length" type="radio" value="Moins 30secondes" class="icheck length">
                                                        </label>
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group radio_questions">
                                                        <label>2. Entre 30secondes et 1minute
                                                            <input name="length" type="radio" value="Entre 30secondes et 1minute" class="icheck length">
                                                        </label>
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group radio_questions">
                                                        <label>3. 1minute ou plus
                                                            <input name="length" type="radio" value="1minute ou plus" class="icheck length">
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div><!-- /step 2-->
                                        {{--End Shooting  --}}

                                        {{--Website Developement--}}
                                        <div class="step">
                                            <h3 class="main_question"><strong>6/7</strong>Website Developement</h3>

                                            <h6 class="main_question" style="font-weight: 400; color: #222;">1. Avez-vous un site web ?</h6>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group radio_questions">
                                                        <label>1. Oui
                                                            <input name="have" type="radio" value="1" class="icheck have">
                                                        </label>
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group radio_questions">
                                                        <label>2. Non
                                                            <input name="have" type="radio" value="0" class="icheck have">
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>

                                            <h6 class="main_question" style="font-weight: 400; color: #222;">2. Voulez vous créer un nouveau site ? </h6>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group radio_questions">
                                                        <label>1. Oui
                                                            <input name="create" type="radio" value="1" class="icheck create">
                                                        </label>
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group radio_questions">
                                                        <label>2. Non
                                                            <input name="create" type="radio" value="0" class="icheck create">
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>

                                            <h6 class="main_question" style="font-weight: 400; color: #222;">3. Voulez vous modérer votre site actuel ? </h6>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group radio_questions">
                                                        <label>1. Oui
                                                            <input name="moderate" type="radio" value="1" class="icheck moderate">
                                                        </label>
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group radio_questions">
                                                        <label>2. Non
                                                            <input name="moderate" type="radio" value="0" class="icheck moderate">
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group textarea_info">
                                                <label>4. Citez l’adresse du site web que vous disposez ? </label>
                                                <input type="text" name="url" class="form-control" id="url" placeholder="">
                                            </div>

                                            <div class="form-group textarea_info">
                                                <label>5. Indiquez la technique de développement utilisée pour la création de votre site web ? </label>
                                                <input type="text" name="techs" class="form-control" id="techs" placeholder="">
                                            </div>

                                            <h6 class="main_question" style="font-weight: 400; color: #222;">6. Quel est le type de votre site web ?</h6>
                                            <div class="row add_bottom_30">
                                              {{--
                                              Application Web
                                              Application ERP
                                              Blog
                                              Portfolio
                                              Site Événementiel
                                              Autre
                                               --}}
                                                <div class="col-sm-6">
                                                    <div class="form-group checkbox_questions">
                                                        <label>
                                                            <input name="typeSite[]" type="checkbox" value="Photos de haute résolution" class="type icheck  ">Application Web
                                                        </label>
                                                    </div>

                                                    <div class="form-group checkbox_questions">
                                                            <label>
                                                                <input name="typeSite[]" type="checkbox" value="Photos 360" class="type icheck  ">Application ERP
                                                            </label>
                                                    </div>

                                                    <div class="form-group checkbox_questions">
                                                            <label>
                                                                <input name="typeSite[]" type="checkbox" value="Photos 360" class="type icheck  ">Portfolio
                                                            </label>
                                                    </div>

                                                </div>



                                                <div class="col-sm-6">
                                                    <div class="form-group checkbox_questions">
                                                        <label>
                                                            <input name="typeSite[]" type="checkbox" value="Photos 360" class="type icheck  ">Blog
                                                        </label>
                                                    </div>


                                                        <div class="form-group checkbox_questions">
                                                            <label>
                                                                <input name="typeSite[]" type="checkbox" value="Photos 360" class="type icheck  ">  Site Événementiel
                                                            </label>
                                                        </div>




                                                        <div class="form-group checkbox_questions">
                                                            <label>
                                                                <input name="typeSite[]" type="checkbox" value="Photos 360" class="icheck" id="site-type">Autre
                                                            </label>
                                                        </div>



                                                </div>


                                            </div><!-- /row-->

                                            <div class="form-group textarea_info">
                                                <textarea name="other_site_type" class="form-control" id="other_site_type" style="height:150px;" placeholder=""></textarea>
                                            </div>


                                            <h6 class="main_question" style="font-weight: 400; color: #222;">7. Avez-vous un nom de domaine ? </h6>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group radio_questions">
                                                        <label>1. Oui
                                                            <input name="sponsored_supports" type="radio" value="1" class="icheck sponsored_supports">
                                                        </label>
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group radio_questions">
                                                        <label>2. Non
                                                            <input name="sponsored_supports" type="radio" value="0" class="icheck sponsored_supports">
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group textarea_info">
                                                <label>8. Citez votre nom de domaine : </label>
                                                <input type="text" name="domain_name" class="form-control" id="domain" placeholder="">
                                            </div>




                                            <h6 class="main_question" style="font-weight: 400; color: #222;">9. Avez-vous un hébergement ? </h6>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group radio_questions">
                                                        <label>1. Oui
                                                            <input name="hosting" type="radio" value="1" class="icheck hosting">
                                                        </label>
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group radio_questions">
                                                        <label>2. Non
                                                            <input name="hosting" type="radio" value="0" class="icheck hosting">
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group textarea_info">
                                                <label>10. Citez le nom  de l’hébergeur ? </label>
                                                <input type="text" name="hosting_name" class="form-control" id="hosting" placeholder="">
                                            </div>


                                            <h6 class="main_question" style="font-weight: 400; color: #222;">11. Précisez les fonctionnalités de votre site web : </h6>
                                            <div class="row add_bottom_30">

                                                {{--
                                                      * Button Appel A l'action
                                                      * Formulaire de reservation
                                                      * formulaire de prise de rendez vous
                                                      * espace des clients
                                                      * Login avec les reseaux sociaux




                                                 --}}
                                                <div class="col-sm-4">
                                                    <div class="form-group checkbox_questions">
                                                        <label>
                                                            <input name="pages[]" type="checkbox" value="Accueil (Home)" class="pages icheck  ">Accueil (Home)
                                                        </label>
                                                    </div>

                                                    <div class="form-group checkbox_questions">
                                                        <label>
                                                            <input name="pages[]" type="checkbox" value="Actualités" class="pages icheck  ">Actualités
                                                        </label>
                                                    </div>
                                                    <div class="form-group checkbox_questions">
                                                        <label>
                                                            <input name="pages[]" type="checkbox" value="A Propos de nous (About us)" class="pages icheck  ">A Propos de nous (About us)
                                                        </label>
                                                    </div>
                                                    <div class="form-group checkbox_questions">
                                                        <label>
                                                            <input name="pages[]" type="checkbox" value="Nos domaines d’activités/compétences" class="pages icheck  ">Nos domaines d’activités/compétences
                                                        </label>
                                                    </div>


                                                </div>

                                                <div class="col-sm-4">

                                                  <div class="form-group checkbox_questions">
                                                      <label>
                                                          <input name="pages[]" type="checkbox" value="Références" class="pages icheck  ">Références
                                                      </label>
                                                  </div>
                                                    <div class="form-group checkbox_questions">
                                                        <label>
                                                            <input name="pages[]" type="checkbox" value="Blogs" class="pages icheck  ">Blogs
                                                        </label>
                                                    </div>
                                                    <div class="form-group checkbox_questions">
                                                        <label>
                                                            <input name="pages[]" type="checkbox" value="Formulaire de prise contact"  class="pages icheck">Formulaire de prise contact
                                                        </label>
                                                    </div>
                                                    <div class="form-group checkbox_questions">
                                                        <label>
                                                            <input name="pages[]" type="checkbox" value="Galeries des produits" class="pages icheck  ">Galeries des produits
                                                        </label>
                                                    </div>


                                                </div>

                                                <div class="col-sm-4">
                                                    <div class="form-group checkbox_questions">
                                                        <label>
                                                            <input name="pages[]" type="checkbox" value="Catalogue"  class="pages icheck">Catalogue
                                                        </label>
                                                    </div>
                                                    <div class="form-group checkbox_questions">
                                                        <label>
                                                            <input name="pages[]" type="checkbox" value="Localisation sur Google Maps"  class="pages icheck">Localisation sur Google Maps
                                                        </label>
                                                    </div>
                                                    <div class="form-group checkbox_questions">
                                                        <label>
                                                            <input name="pages[]" type="checkbox" value="Hosting plan"  id="other-page" class="icheck">Autre
                                                        </label>
                                                    </div>

                                                </div>
                                            </div><!-- /row-->
                                            <div class="form-group textarea_info">
                                                <textarea name="other_pages" class="form-control" id="other_pages" style="height:150px;" placeholder=""></textarea>
                                            </div>



                                            <h6 class="main_question" style="font-weight: 400; color: #222;">12. Quelle langue choisissez-vous pour votre site web </h6>
                                            <div class="row add_bottom_30">

                                                <div class="col-sm-3">
                                                    <div class="form-group checkbox_questions">
                                                        <label>
                                                            <input name="languages[]" type="checkbox" value="Français" class="languages icheck  ">Français
                                                        </label>
                                                    </div>


                                                </div>

                                                <div class="col-sm-3">
                                                    <div class="form-group checkbox_questions">
                                                        <label>
                                                            <input name="languages[]" type="checkbox" value="Anglais" class="languages icheck  ">Anglais
                                                        </label>
                                                    </div>


                                                </div>

                                                <div class="col-sm-3">
                                                    <div class="form-group checkbox_questions">
                                                        <label>
                                                            <input name="languages[]" type="checkbox" value="Arabe" class="languages icheck  ">Arabe
                                                        </label>
                                                    </div>


                                                </div>

                                                <div class="col-sm-3">
                                                    <div class="form-group checkbox_questions">
                                                        <label>
                                                            <input type="checkbox"  id="other-language" class="icheck  ">Autre
                                                        </label>
                                                    </div>


                                                </div>
                                            </div><!-- /row-->
                                            <div class="form-group textarea_info">
                                                <textarea name="other_languages" class="form-control" id="other_languages" style="height:150px;" placeholder=""></textarea>
                                            </div>


                                            <h6 class="main_question" style="font-weight: 400; color: #222;">13. Comment nous allons obtenir le contenu nécessaire à la création de votre site web ?</h6>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group radio_questions">
                                                        <label>1. Vous engagez à nous fournir le contenu
                                                            <input name="content" type="radio" value="1" class="icheck content">
                                                        </label>
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group radio_questions">
                                                        <label>2. Data Deep prend en charge la création de contenu
                                                            <input name="content" type="radio" value="0" class="icheck content">
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>

                                            <h6 class="main_question" style="font-weight: 400; color: #222;">14. Quel est le type du contenu de votre site web ?</h6>
                                            <div class="row add_bottom_30">

                                                <div class="col-sm-4">
                                                    <div class="form-group checkbox_questions">
                                                        <label>
                                                            <input name="content_type[]" type="checkbox" value="Contenu textuel" class="content_type icheck  ">Contenu textuel
                                                        </label>
                                                    </div>


                                                </div>

                                                <div class="col-sm-4">
                                                    <div class="form-group checkbox_questions">
                                                        <label>
                                                            <input name="content_type[]" type="checkbox" value="Contenu visuel" class="content_type icheck  ">Contenu visuel
                                                    </div>

                                                </div>

                                                <div class="col-sm-4">
                                                    <div class="form-group checkbox_questions">
                                                        <label>
                                                            <input name="content_type[]" type="checkbox" value="Contenu audio-visuel" class="content_type icheck  ">Contenu audio-visuel
                                                        </label>
                                                    </div>
                                                </div>
                                            </div><!-- /row-->


                                            <h6 class="main_question" style="font-weight: 400; color: #222;">15. Que sera le thème de votre site web ?</h6>
                                            <div class="row add_bottom_30">

                                                <div class="col-sm-6">
                                                    <div class="form-group checkbox_questions">
                                                        <label>
                                                            <input name="themeSite[]" type="checkbox" value="Modern" class="theme icheck  ">Modern
                                                        </label>
                                                    </div>
                                                    <div class="form-group checkbox_questions">
                                                        <label>
                                                            <input name="themeSite[]" type="checkbox" value="Classic" class="theme icheck  ">Classic
                                                    </div>
                                                    <div class="form-group checkbox_questions">
                                                        <label>
                                                            <input name="themeSite[]" type="checkbox" value="Bright & Colorful" class="theme icheck  ">Bright & Colorful
                                                        </label>
                                                    </div>

                                                </div>

                                                <div class="col-sm-6">
                                                    <div class="form-group checkbox_questions">
                                                        <label>
                                                            <input name="themeSite[]" type="checkbox" value="Luxe" class="theme icheck  ">Luxe
                                                        </label>
                                                    </div>
                                                    <div class="form-group checkbox_questions">
                                                        <label>
                                                            <input name="themeSite[]" type="checkbox" value="Artistique" class="theme icheck  ">Artistique
                                                    </div>
                                                    <div class="form-group checkbox_questions">
                                                        <label>
                                                            <input name="themeSite[]" type="checkbox" value="Simple" class="theme icheck  ">Simple
                                                        </label>
                                                    </div>

                                                </div>

                                            </div><!-- /row-->


                                        </div><!-- /step 2-->
                                        {{--End Website Developement --}}

                                        {{--Graphic Design--}}
                                        <div class="step">
                                            <h3 class="main_question"><strong>7/7</strong>Graphic Design</h3>

                                            <h6 class="main_question" style="font-weight: 400; color: #222;">1. Souhaitez-vous créer un logo pour votre société ? </h6>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group radio_questions">
                                                        <label>1. Oui
                                                            <input name="logo" type="radio" value="1" class="icheck logo">
                                                        </label>
                                                    </div>
                                                </div>

                                                <div class="col-md-4">
                                                    <div class="form-group radio_questions">
                                                        <label>2.Non
                                                            <input name="logo" type="radio" value="0" class="icheck logo">
                                                        </label>
                                                    </div>
                                                </div>

                                                <div class="col-md-4">
                                                    <div class="form-group radio_questions">
                                                        <label>3.Peut-être
                                                            <input name="logo" type="radio" value="2" class="icheck logo">
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>

                                            <h6 class="main_question" style="font-weight: 400; color: #222;">2. Souhaitez-vous élaborer des cartes de visites ? </h6>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group radio_questions">
                                                        <label>1. Oui
                                                            <input name="cards" type="radio" value="1" class="icheck cards">
                                                        </label>
                                                    </div>
                                                </div>

                                                <div class="col-md-4">
                                                    <div class="form-group radio_questions">
                                                        <label>2.Non
                                                            <input name="cards" type="radio" value="0" class="icheck cards">
                                                        </label>
                                                    </div>
                                                </div>

                                                <div class="col-md-4">
                                                    <div class="form-group radio_questions">
                                                        <label>3.Peut-être
                                                            <input name="cards" type="radio" value="2" class="icheck cards">
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>

                                            <h6 class="main_question" style="font-weight: 400; color: #222;">3. Souhaitez-vous concevoir des flyers ? </h6>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group radio_questions">
                                                        <label>1. Oui
                                                            <input name="flyers" type="radio" value="1" class="icheck flyers">
                                                        </label>
                                                    </div>
                                                </div>

                                                <div class="col-md-4">
                                                    <div class="form-group radio_questions">
                                                        <label>2.Non
                                                            <input name="flyers" type="radio" value="0" class="icheck flyers">
                                                        </label>
                                                    </div>
                                                </div>

                                                <div class="col-md-4">
                                                    <div class="form-group radio_questions">
                                                        <label>3.Peut-être
                                                            <input name="flyers" type="radio" value="2" class="icheck flyers">
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>

                                            <h6 class="main_question" style="font-weight: 400; color: #222;">4. Souhaitez-vous concevoir des catalogues ? </h6>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group radio_questions">
                                                        <label>1. Oui
                                                            <input name="catalogue" type="radio" value="1" class="icheck catalogue">
                                                        </label>
                                                    </div>
                                                </div>

                                                <div class="col-md-4">
                                                    <div class="form-group radio_questions">
                                                        <label>2.Non
                                                            <input name="catalogue" type="radio" value="0" class="icheck catalogue">
                                                        </label>
                                                    </div>
                                                </div>

                                                <div class="col-md-4">
                                                    <div class="form-group radio_questions">
                                                        <label>3.Peut-être
                                                            <input name="catalogue" type="radio" value="2" class="icheck catalogue">
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>

                                            <h6 class="main_question" style="font-weight: 400; color: #222;">5. Souhaitez-vous établir la charte graphique pour votre société ? </h6>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group radio_questions">
                                                        <label>1. Oui
                                                            <input name="chart" type="radio" value="1" class="icheck chart">
                                                        </label>
                                                    </div>
                                                </div>

                                                <div class="col-md-4">
                                                    <div class="form-group radio_questions">
                                                        <label>2.Non
                                                            <input name="chart" type="radio" value="0" class="icheck chart">
                                                        </label>
                                                    </div>
                                                </div>

                                                <div class="col-md-4">
                                                    <div class="form-group radio_questions">
                                                        <label>3.Peut-être
                                                            <input name="chart" type="radio" value="2" class="icheck chart">
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>

                                            <h6 class="main_question" style="font-weight: 400; color: #222;">6. Souhaitez-vous établir une présentation PPT de votre société ? </h6>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group radio_questions">
                                                        <label>1. Oui
                                                            <input name="ppt" type="radio" value="1" class="icheck ppt">
                                                        </label>
                                                    </div>
                                                </div>

                                                <div class="col-md-4">
                                                    <div class="form-group radio_questions">
                                                        <label>2.Non
                                                            <input name="ppt" type="radio" value="0" class="icheck ppt">
                                                        </label>
                                                    </div>
                                                </div>

                                                <div class="col-md-4">
                                                    <div class="form-group radio_questions">
                                                        <label>3.Peut-être
                                                            <input name="ppt" type="radio" value="2" class="icheck ppt">
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group textarea_info">
                                                <label>7. Souhaitez-vous concevoir une affiche publicitaire de votre société /produits ? </label>
                                                <textarea name="affiche" id="affiche" class="form-control" style="height:75px;" placeholder=""></textarea>
                                            </div>
                                        </div>
                                        {{--End Graphic Design--}}


                                        <div class="submit step">
                                            <h3 class="main_question"><strong>3/4</strong>Conclusion:</h3>


                                        </div><!-- /step 3-->
                                    </div><!-- /middle-wizard -->
                                    <div id="bottom-wizard">
                                        <button type="button" name="backward" class="backward">Backward </button>
                                        <button type="button" name="forward" class="forward">Forward</button>
                                        <button type="submit" name="process" class="submit">Submit</button>
                                    </div><!-- /bottom-wizard -->
                                </form>
                            </div><!-- /Wizard container -->

                        </div><!-- /col -->
                    </div><!-- /row -->
                </div><!-- /TAB 1:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: -->





            </div><!-- /tab content -->
        </div><!-- /container-fluid -->
    </div><!-- /wrapper_in -->
    </div><!-- /main_container -->




    <!-- Jquery-->
    <script src="{{asset('brief/js/jquery-3.2.1.min.js')}}"></script>
    <!-- Common script -->
    <script src="{{asset('brief/js/common_scripts.js')}}"></script>
    <!-- Theme script -->
    <script src="{{asset('brief/js/functions_no_side_panel.js')}}"></script>





    <!-- Latest compiled and minified JavaScript -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/js/bootstrap-select.min.js"></script>


    <script type="text/javascript">

    	$('form#wrapped').attr('action', '{{route('handleAddBriefClient')}}');
        $(function() {
            $('.selectpicker').selectpicker();




              $('input#page').on('ifChecked',function(){

              $('#other_accounts').show();

              });

              $('input#types').on('ifChecked',function(){

              $('#other_types').show();

              });

              $('input#site-type').on('ifChecked',function(){

              $('#other_site_type').show();

              });

              $('input#other-page').on('ifChecked',function(){

              $('#other_pages').show();

              });

              $('input#other-language').on('ifChecked',function(){

              $('#other_languages').show();

              });
        });
    </script>




</body>

</html>
