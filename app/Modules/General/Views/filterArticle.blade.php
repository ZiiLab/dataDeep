@foreach($articles as $article)
<figure class="effect-ruby {{$article->category->name}}">
    <img src="{{asset($article->image)}}" alt="{{$article->title}}" style="height: 360px;" />
    <figcaption>
        <h3><span>{{$article->title}}</span></h3>
        <p>{{$article->description}}</p>
        <a href="{{route('showArticle','_'.$article->id)}}">View more</a>
    </figcaption>
</figure>
@endforeach
