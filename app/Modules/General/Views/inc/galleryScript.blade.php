<script type="text/javascript">
    $(document).on('ready', function() {
        $(".regular").slick({
            dots: true,
            infinite: true,
            slidesToShow: 2,
            slidesToScroll: 2
        });

    });
</script>


<script type="text/javascript">
    var offset;
    var category;
    $(document).on('click', '#show-more-article', function(event) {
        event.preventDefault();
        var base_url = '{!! url(' / ') !!}';

        var data;
        var filterCat;
        offset = $('#show-more-article').data('offset');
        filterCat = $('#show-more-article').data('category', );
        data = {
            'offset': offset,
            'categoryId': filterCat
        }

        $('#show-more-article').html('<span class="spinner"><i class="fa fa-spinner fa-spin"></i></span>');

        $.post('{{route('apiHandleGategoryFilter')}}', {
                    '_token': $('meta[name=csrf-token]').attr('content'),
                    data

                })
            .done(function(res) {


                offset++;
                $('#show-more-article').data("offset", offset);
                $('#show-more-article').html('Show More Article');
                if (res.status === 201 || res.html === "" || res.rows < 6) {
                    $('#show-more-article').hide();
                }

                $('#articles').append(res.html).animate({
                    duration: 750,
                    easing: "linear"
                });


            })



    })

    $(function() {

        $('.category').on('click', function() {

            var data;
            var filterCat = $(this).data('category');
            var offsetCat = $(this).data('offset');

            data = {
                'categoryId': filterCat,
                'offset': offsetCat
            };


            var base_url = '{!! url('/') !!}';


            $('#loader').removeClass('hide');

            $('#articles').html("");
            $('#show-more-article').hide();

            $.post('{{route('apiHandleGategoryFilter')}}', {
                        '_token': $('meta[name=csrf-token]').attr('content'),
                        data

                    })
                .done(function(res) {

                    $('#show-more-article').show();
                    // $('#loader').addClass('hide');


                    if (res.status === 201 || res.html === "" || res.rows < 6) {
                        $('#show-more-article').hide();
                    }


                    $('#show-more-article').data('category', filterCat);
                    $('#show-more-article').data('offset', 1);
                    $('#loader').addClass('hide');
                    $('#articles').addClass('hide');

                    if (res.rows == 0) {
                      $('#articles').addClass('hide');
                      var div = $('<div></div>');
                      var src = base_url+'/dipper/img/oops.png';
                      div.html('<img src="'+src+'" />')
                      div.append('<h6>Looks like there is no articles in this section yet.</h6>');

                      div.addClass('message');
                      $('#articles').fadeIn().html(div);
                    }else {
                          $('#articles').fadeIn().html(res.html);
                    }


                })

        });


    });
</script>
