<script type="text/javascript">


  $('#test').on('click',function (event) {
    event.preventDefault();
    $('#myDropdown').toggle('show');

  });

  window.onclick = function(event) {
    if (!event.target.matches('.dropbtn')) {
      var dropdowns = document.getElementsByClassName("dropdown-content");
      var i;
      for (i = 0; i < dropdowns.length; i++) {
        var openDropdown = dropdowns[i];
        if (openDropdown.classList.contains('show')) {
          openDropdown.classList.remove('show');
        }
      }
    }
  }

</script>
<script type="text/javascript">
    $(function() {
      $('#post-comment').on('click',function() {

        @if (Auth::check())
        var content = $('#content-comment').val();
        var articleId = $('#articleId').val();
        var data = {
          'content' : content,
          'articleId' : articleId
        }

        $.post('{{route('apiHandleAddComment')}}',
          {
              '_token': $('meta[name=csrf-token]').attr('content'), data : data

          })
          .done(function (res) {

              var userComment;

              userComment = '<div class="media comments">';
              userComment +='<img class="d-flex rounded-circle avatar z-depth-1-half mr-3" src="{{asset(Auth::user()->image)}}" alt="Avatar">';
              userComment +='<div class="media-body">';
              userComment +='<h5 class="mt-0 font-weight-bold blue-text">{{Auth::user()->first_name}}</h5>';
              userComment +='<span>'+res.comment.content+'</span>';
              userComment +='<a href="#" class="show-more-replay" data-replies="'+res.comment.id+'"><span class="no-replies">No replies</span></a>'
              userComment +='</div>';
              userComment +='<br>';
              userComment +='<a href="#" class="respense" data-comment='+res.comment.id+'><i class="fa fa-reply"></i></a>';
              userComment +='</div>';
              userComment +='<div id="comment-replay'+res.comment.id+'"></div>'
              userComment +='<div id="comment-respense'+res.comment.id+'"></div>'

              $('#article-comments').append(userComment);
              $('#content-comment').val('');
          })

          @else
            $('#loginModal').modal('show')

        @endif
      });

    var commentId;
    $(document).on('click','.respense',function(event) {
          event.preventDefault();


    @if (Auth::check())
              $('#comment-respense'+commentId).html(' ');
              commentId = $(this).data("comment");
              var commentText ='<div class="media mt-3 shadow-textarea">';
                  commentText +='<div class="media-body">';
                  commentText +='<div class="form-group basic-textarea rounded-corners">';
                  commentText +='<input type="hidden" id = "comment_id" name="comment_id" value="'+commentId+'">';
                  commentText +='<textarea class="form-control z-depth-1" id="content-replay" rows="3" placeholder="Write your comment..."></textarea>';
                  commentText +='<button type="button" id = "comment" class="btn btn-info pull-right">Post</button>';
                  commentText +='</div>';
                  commentText +='</div>';
                  commentText +='</div>';
              $('#comment-respense'+commentId).append(commentText);
              @else
                $('#loginModal').modal('show')

            @endif
        });

        $(document).on('click','#comment',function() {

          @if (Auth::check())
          var content = $('#content-replay').val();
          var comment_id = $('#comment_id').val();
          var data = {
            'content' : content,
            'commentId' : comment_id
          }


          $.post('{{route('apiHandleAddComment')}}',
            {
                '_token': $('meta[name=csrf-token]').attr('content'), data

            })
            .done(function (res) {


              var replayComment;
              replayComment='<div class="media mt-3 shadow-textarea c-replay" style="margin-left: 50px;">';
              replayComment+='<img class="d-flex rounded-circle avatar z-depth-1-half mr-3" src="{{asset(Auth::user()->image)}}" alt="Generic placeholder image">';
              replayComment+='<div class="media-body">';
              replayComment+='<h5 class="mt-0 font-weight-bold blue-text">{{Auth::user()->first_name}}</h5>';
              replayComment+='<div class="form-group basic-textarea rounded-corners">';
              replayComment+='<span>'+res.comment.content+'</span>';
              replayComment+='</div>';
              replayComment+='</div>';
              replayComment+='</div>';
              $('#comment-replay'+commentId).append(replayComment);
              $('#content-replay').val('');
              $('#comment-respense'+commentId).html(' ');

            })
            @else
              $('#loginModal').modal('show')

          @endif

    });
  });
</script>

<script type="text/javascript">
      $(document).on('click','#show-more-comment',function(event) {
          event.preventDefault();
          var base_url = '{!! url('/') !!}';
          var offset;
          var data;
          offset = $('#show-more-comment').data('offset');
            var articleId = $('#articleId').val();
          data = {
            'offset' : offset,
            'articleId':articleId
          }

          $.post('{{route('apiHandleShowMoreComment')}}',
            {
                '_token': $('meta[name=csrf-token]').attr('content'), data

            })
            .done(function (res) {

              if (typeof(res.event) != "undefined" || res.comments.length === 0 || res.row < 3) {
                   $('#show-more-comment').hide();
              }
              offset++;
              $('#show-more-comment').data( "offset", offset );



              $.each(res.comments, function( key, comment ) {



                var img = comment.author.image;
                    userComments = '<div class="media comments">';
                    userComments +='<img class="d-flex rounded-circle avatar z-depth-1-half mr-3" src="'+base_url+'/'+img+'" alt="Avatar">';
                    userComments +='<div class="media-body">';
                    userComments +='<h5 class="mt-0 font-weight-bold blue-text">'+comment.author.first_name+'</h5>';
                    userComments +='<span>'+comment.content+'</span>';
                    if (comment.comments.length > 0 ) {
                      userComments +='<a href="#" class="show-more-replay" data-replies="'+comment.id+'">Show Replies <span>('+comment.comments.length+')</span></a>';
                    }else {
                      userComments +='<a href="#" class="show-more-replay" data-replies="'+comment.id+'"><span class="no-replies">No replies</span></a>';
                    }

                    userComments +='</div>';
                    userComments +='<br>';
                    userComments +='<a href="#" class="respense" data-comment='+comment.id+'><i class="fa fa-reply"></i></a>';
                    userComments +='</div>';
                    userComments +='<div id="comment-replay'+comment.id+'"></div>'
                    userComments +='<div id="comment-respense'+comment.id+'"></div>'

                    $('#article-comments').append(userComments);
                });

            })



      })
</script>

<script type="text/javascript">



      $(document).on('click','.show-more-replay',function(event) {
        event.preventDefault();
        var commentId;
        var base_url = '{!! url('/') !!}';
        commentId = $(this).data('replies');
        var data = {
          'commentId' : commentId
        }

        $.post('{{route('apiHandleGetCommentReplies')}}',
          {
              '_token': $('meta[name=csrf-token]').attr('content'), data

          })
          .done(function (res) {

            $.each(res.replies, function( key, replay ) {

              var img = replay.author.image;

                        var replayComment;
                        replayComments='<div class="media mt-3 shadow-textarea c-replay" style="margin-left: 50px;">';
                        replayComments+='<img class="d-flex rounded-circle avatar z-depth-1-half mr-3" src="'+base_url+'/'+img+'" alt="Generic placeholder image">';
                        replayComments+='<div class="media-body">';
                        replayComments+='<h5 class="mt-0 font-weight-bold blue-text">'+replay.author.first_name+'</h5>';
                        replayComments+='<div class="form-group basic-textarea rounded-corners">';
                        replayComments+='<span>'+replay.content+'</span>';
                        replayComments+='</div>';
                        replayComments+='</div>';
                        replayComments+='</div>';
                        $('#comment-replay'+commentId).append(replayComments);
              });

          })

      });

</script>
