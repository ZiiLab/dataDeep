<?php

Route::group(['module' => 'Brif', 'middleware' => ['web'], 'namespace' => 'App\Modules\Brif\Controllers'], function() {

    Route::resource('Brif', 'BrifController');


    Route::post('client_brief','BrifController@handleAddBriefClient')->name('handleAddBriefClient');
});
