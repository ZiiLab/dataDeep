<?php

namespace App\Modules\Brif\Models;

use Illuminate\Database\Eloquent\Model;

class Development extends Model {

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'developments';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'have',
        'create',
        'moderate',
        'url',
        'techs',
        'type',
        'domain',
        'hosting',
        'pages',
        'languages',
        'content',
        'content_type',
        'theme',
        'business_id',
        'other_languages',
        'other_site_type',
        'other_pages',
        'domain_name',
        'hosting_name'
    ];



    public function business() {

        return $this->belongsTo('App\Modules\Brif\Models\General');
    }

    public function getHaveAttribute($value)
    {
          $answer = 'Non';
          if ($value == 1) {
              $answer = 'Oui';
          }
          return $answer;
    }
    public function getCreateAttribute($value)
    {
          $answer = 'Non';
          if ($value == 1) {
              $answer = 'Oui';
          }
          return $answer;
    }

    public function getModerateAttribute($value)
    {
          $answer = 'Non';
          if ($value == 1) {
              $answer = 'Oui';
          }
          return $answer;
    }

    public function getDomainAttribute($value)
    {
          $answer = 'Non';
          if ($value == 1) {
              $answer = 'Oui';
          }
          return $answer;
    }

    public function getHostingAttribute($value)
    {
          $answer = 'Non';
          if ($value == 1) {
              $answer = 'Oui';
          }
          return $answer;
    }

    public function getContentAttribute($value)
    {
          $answer = 'Vous engagez à nous fournir le contenu';
          if ($value == 1) {
              $answer = 'Data Deep prend en charge la création de contenu';
          }
          return $answer;
    }
}
