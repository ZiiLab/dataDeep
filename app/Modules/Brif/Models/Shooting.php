<?php

namespace App\Modules\Brif\Models;

use Illuminate\Database\Eloquent\Model;

class Shooting extends Model {

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'shootings';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'type',
        'photo_types',
        'categories',
        'number',
        'theme',
        'length',
        'business_id'
    ];



    public function business() {

        return $this->belongsTo('App\Modules\Brif\Models\General');
    }

}
