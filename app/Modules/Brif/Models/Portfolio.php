<?php

namespace App\Modules\Brif\Models;

use Illuminate\Database\Eloquent\Model;

class Portfolio extends Model {

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'portfolios';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'service',
        'va',
        'values',
        'audience',
        'market',
        'concurrents',
        'business_id'
    ];


    public function business() {

        return $this->belongsTo('App\Modules\Brif\Models\General');
    }


}
