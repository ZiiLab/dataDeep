<?php

namespace App\Modules\Blog\Models;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'articles';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'description',
        'content',
        'image',
        'status',
        'user_id',
        'category_id',
    ];

    protected $casts = [
        'tags' => 'array'
    ];

    public function author(){
        return  $this->hasOne('App\Modules\Blog\Models\User','id','user_id');
    }

    public function category(){
        return  $this->hasOne('App\Modules\Blog\Models\Category','id','category_id');
    }


    public function comments(){
        return  $this->hasMany('App\Modules\Blog\Models\Comment');
    }

    public function clicks(){
        return  $this->hasMany('App\Modules\Blog\Models\Click');
    }

    public function tags()
    {
        return $this->belongsToMany(
            'App\Modules\Blog\Models\Tag',
            'article_tags',
            'article_id',
            'tag_id'
        )->withTimestamps();
    }

    public function addTag($tagParam)
    {
        if (is_string($tagParam)) {
            $tag = Tag::where('name', $tagParam)->first();
        }
        if (!$tag){
            $newTag = Tag::create([
                'name' => $tagParam
            ]);

            $this->tags()->attach($newTag);
        }
        else
            $this->tags()->attach($tag);
    }

//    public function revokeTag($tag)
//    {
//        if (is_string($tag)) {
//            $tag = Tag::where('name', $tag)->get();
//        }
//        if (!$tag) return false;
//        $this->tags()->detach($tag);
//    }



}
