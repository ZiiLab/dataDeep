<?php

namespace App\Modules\Blog\Models;

use Illuminate\Database\Eloquent\Model;

class Click extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'clicks';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'article_id',
        'user_id'
    ];

    public function user(){
        return  $this->belongsTo('App\Modules\Blog\Models\User');
    }

    public function article(){
        return  $this->belongsTo('App\Modules\Blog\Models\Article');
    }

}
