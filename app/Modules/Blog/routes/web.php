<?php

Route::group(['module' => 'Blog', 'middleware' => ['web'], 'namespace' => 'App\Modules\Blog\Controllers'], function() {

    Route::get('/login', 'BlogController@showDashboardLogin')->name('showDashboardLogin');
    Route::post('/login', 'BlogController@handleDashboardLogin')->name('handleDashboardLogin');
    Route::get('/logout', 'BlogController@handleDashboardLogout')->name('handleDashboardLogout');


    Route::group(['prefix' => 'dashboard', 'middleware' => ['user']],function(){

        Route::get('/', 'BlogController@showDashboard')->name('showDashboard');
        Route::get('/students', 'BlogController@showManagementStudentsInscription')->name('showManagementStudentsInscription');
        Route::group(['prefix' => 'users', 'middleware' => ['user']],function(){

            Route::get('/', 'BlogController@showUsers')->name('showUsers');
            Route::get('/{id}', 'BlogController@showUser')->name('showUser');
            Route::get('/ban/{id}', 'BlogController@handleBanUser')->name('handleBanUser');
            Route::get('/unban/{id}', 'BlogController@handleUnbanUser')->name('handleUnbanUser');

        });

        Route::group(['prefix' => 'articles', 'middleware' => ['user']],function(){

            Route::get('/', 'BlogController@showArticles')->name('showArticles');
            Route::get('/add', 'BlogController@showAddArticle')->name('showAddArticle');
            Route::get('/edit/{id}', 'BlogController@showEditArticle')->name('showEditArticle');
            Route::post('/add', 'BlogController@handleAddArticle')->name('handleAddArticle');
            Route::post('/edit/{id}', 'BlogController@handleEditArticle')->name('handleEditArticle');
            Route::get('getvisitors', 'BlogController@getActiveUser')->name('getActiveUser');



        });

          Route::get('/studentsDev', 'BlogController@apiHundleManagmentStudent')->name('apiHundleManagmentStudent');
    });

    });
