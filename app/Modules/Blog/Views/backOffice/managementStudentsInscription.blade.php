@extends('backOffice.layoutAnalytics')



@section('content')

<main class="content">
    <div class="container-fluid p-0">

      <div class="row">

          <div class="col-12 col-lg-12 col-xl-8 d-flex">

              <div class="card flex-fill">
                  <div class="card-header">
                      <div class="card-actions float-right">
                          <div class="dropdown show">
                              <a href="#" data-toggle="dropdown" data-display="static">
                                  <i class="align-middle" data-feather="more-horizontal"></i>
                              </a>

                              <div class="dropdown-menu dropdown-menu-right">
                                  <a class="dropdown-item" href="#">Action</a>
                                  <a class="dropdown-item" href="#">Another action</a>
                                  <a class="dropdown-item" href="#">Something else here</a>
                              </div>
                          </div>
                      </div>
                      <h2 class="card-title mb-0">Dev Students</h2>
                  </div>
                  <table id="datatables-dev-student" class="table table-striped my-0">
                      <thead>
                          <tr>

                              <th class="">Name</th>
                              <th class="d-none d-xl-table-cell ">Email</th>
                              <th class="d-none d-xl-table-cell ">Phone</th>
                              <th class="d-none d-xl-table-cell ">Address</th>
                              <th class="d-none d-xl-table-cell ">Age</th>
                              <th class="d-none d-xl-table-cell ">Skills</th>
                              <th class="d-none d-xl-table-cell ">Action</th>

                          </tr>
                      </thead>
                      <tbody>

                          @foreach ($studentsDev as $student)
                          <tr>

                              <td>{{$student->full_name}}</td>
                              <td>{{$student->email}}</td>
                              <td>{{$student->phone}}</td>
                              <td>{{$student->address}}</td>
                              <td>{{$student->age}}</td>
                              <td>
                                  @foreach ($student->skills as $skill)
                                  <span>{{$skill->label}}</span><br>
                                  @endforeach
                              </td>
                              <td><a href="#" class="managementStudentDev" data-status='1' data-id='{{$student->id}}'>Accept</a>
                                  <a href="#" class="managementStudentDev" data-status='2' data-id='{{$student->id}}'>Refuser</a> </td>
                          </tr>

                          @endforeach
                      </tbody>
                  </table>
              </div>
          </div>
          <div class="col-12 col-lg-12 col-xl-2 d-flex" style="padding: 0px;">
              <div class="panel panel-primary" style="width: 100%;">
                  <div class="panel-heading">Accepted</div>
                  <div class="panel-body" style="padding: 0px;">
                    <table id="datatables-dev-student-accepted" class="table table-striped my-0">
                        <thead>
                          <tr>
                            <th>name</th>
                            <th>action</th>
                          </tr>
                        </thead>
                        <tbody>
                @foreach ($studentsDevAccepted as $student)
                  <tr>
                    <td>{{$student->full_name}}</td>
                    <td> <a href="#" class="managementStudentDev" data-status='2' data-id='{{$student->id}}'>Refuser</a></td>

                  </tr>
                @endforeach


                        </tbody>
                      </table>


                  </div>
              </div>
          </div>
          <div class="col-12 col-lg-12 col-xl-2 d-flex" style="padding: 0px;">
              <div class="panel panel-danger"  style="width: 100%;">
                  <div class="panel-heading">Refused</div>
                  <div class="panel-body" style="padding: 0px;">

                    <table id="datatables-dev-student-refused" class="table table-striped my-0">
                        <thead>
                          <tr>
                            <th>name</th>
                            <th>action</th>

                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>Mohamed Barhoumi</td>
                            <td>Doe</td>

                          </tr>
                          <tr>
                            <td>Mary</td>
                            <td>Moe</td>

                          </tr>
                          <tr>
                            <td>July</td>
                            <td>Dooley</td>

                          </tr>
                        </tbody>
                      </table>
                  </div>
              </div>
          </div>
      </div>
    <div class="row">
      <div class="col-12 col-lg-12 col-xl-12 d-flex">

          <div class="card flex-fill">
              <div class="card-header">
                  <div class="card-actions float-right">
                      <div class="dropdown show">
                          <a href="#" data-toggle="dropdown" data-display="static">
                              <i class="align-middle" data-feather="more-horizontal"></i>
                          </a>

                          <div class="dropdown-menu dropdown-menu-right">
                              <a class="dropdown-item" href="#">Action</a>
                              <a class="dropdown-item" href="#">Another action</a>
                              <a class="dropdown-item" href="#">Something else here</a>
                          </div>
                      </div>
                  </div>
                  <h2 class="card-title mb-0">Marketing Students</h2>
              </div>
              <table id="datatables-marketing-student" class="table table-striped my-0">
                  <thead>
                      <tr>

                          <th class="">Name</th>
                          <th>Email</th>
                          <th>Phone</th>
                          <th>Address</th>
                          <th>Age</th>
                          <th>Skills</th>
                          <th>Action</th>

                      </tr>
                  </thead>
                  <tbody>

                      @foreach ($studentsMarketing as $student)
                      <tr>

                          <td>{{$student->full_name}}</td>
                          <td class="">{{$student->email}}</td>
                          <td>{{$student->phone}}</td>
                          <td>{{$student->address}}</td>
                          <td>{{$student->age}}</td>
                          <td>
                              @foreach ($student->skills as $skill)
                              <span>{{$skill->label}}</span><br>
                              @endforeach
                          </td>

                          <td><a href="#" class="managementStudentDev" data-status='1' data-id='{{$student->id}}'>Accept</a>
                              <a href="#" class="managementStudentDev" data-status='2' data-id='{{$student->id}}'>Refuser</a> </td>
                      </tr>
                      @endforeach

                  </tbody>
              </table>
          </div>
      </div>
    </div>


  </div>
</main>
<script type="text/javascript">

$('.managementStudentDev').on('click',function () {
event.preventDefault();
console.log(1);
var studentId = $(this).data('id');
var status    = $(this).data('status');
console.log(studentId,status);
$.get("{{ route('apiHundleManagmentStudent')}}?studentId="+studentId+"&status="+status).done(function (res) {
      if (status == 1) {
          $('#datatables-dev-student-accepted head').append('')
      }
});
})

</script>
@endsection
