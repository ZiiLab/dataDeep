@extends('backOffice.layoutAnalytics')



@section('content')

  <main class="content">
      <div class="container-fluid p-0">

        <div class="row">
          <div class="col-lg-6 col-xl-5 d-flex">
            <div class="w-100">
              <div class="row">
                <div class="col-sm-6">
                  <div class="card flex-fill">
                    <div class="card-header">
                      <span class="badge badge-primary float-right">Today</span>
                      <h5 class="card-title mb-0">Visitors</h5>
                    </div>
                    <div class="card-body my-2">
                      <div class="row d-flex align-items-center mb-4">
                        <div class="col-8">
                          <h2 class="d-flex align-items-center mb-0 font-weight-light" id="active-visitor">
                            {{$activeVisitors}}
                          </h2>
                        </div>
                        {{-- <div class="col-4 text-right">
                          <span class="text-muted">57%</span>
                        </div> --}}
                      </div>

                      {{-- <div class="progress progress-sm shadow-sm mb-1">
                        <div class="progress-bar bg-primary" role="progressbar" style="width: 57%"></div>
                      </div> --}}
                    </div>
                  </div>
                  <div class="card flex-fill">
                    <div class="card-header">
                      <span class="badge badge-info float-right">Monthly</span>
                      <h5 class="card-title mb-0">Page views</h5>
                    </div>
                    <div class="card-body my-2">
                      <div class="row d-flex align-items-center mb-4">
                        <div class="col-8">
                          <h2 class="d-flex align-items-center mb-0 font-weight-light">
                          {{$pageviews}}
                          </h2>
                        </div>
                        {{-- <div class="col-4 text-right">
                          <span class="text-muted">64%</span>
                        </div> --}}
                      </div>

                      {{-- <div class="progress progress-sm shadow-sm mb-1">
                        <div class="progress-bar bg-info" role="progressbar" style="width: 64%"></div>
                      </div> --}}
                    </div>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="card flex-fill">
                    <div class="card-header">
                      <span class="badge badge-warning float-right">Monthly</span>
                        <h5 class="card-title mb-0">{{$usersType->pluck('visitorType')[0]}}</h5>
                    </div>
                    <div class="card-body my-2">
                      <div class="row d-flex align-items-center mb-4">
                        <div class="col-8">
                          <h2 class="d-flex align-items-center mb-0 font-weight-light">
                            {{$usersType->pluck('nombres')[0]}}
                          </h2>
                        </div>
                        <div class="col-4 text-right">
                          <span class="text-muted">{{round($usersType->pluck('percentage')[0],2)}}%</span>
                        </div>
                      </div>

                      <div class="progress progress-sm shadow-sm mb-1">
                        <div class="progress-bar bg-warning" role="progressbar" style="width: {{$usersType->pluck('percentage')[0]}}%"></div>
                      </div>
                    </div>
                  </div>
                  <div class="card flex-fill">
                    <div class="card-header">
                      <span class="badge badge-success float-right">Monthly</span>
                      <h5 class="card-title mb-0">{{$usersType->pluck('visitorType')[1]}}</h5>
                    </div>
                    <div class="card-body my-2">
                      <div class="row d-flex align-items-center mb-4">
                        <div class="col-8">
                          <h2 class="d-flex align-items-center mb-0 font-weight-light">
                              {{$usersType->pluck('nombres')[1]}}
                          </h2>
                        </div>
                        <div class="col-4 text-right">
                          <span class="text-muted">{{round($usersType->pluck('percentage')[1],2)}}%</span>
                        </div>
                      </div>

                      <div class="progress progress-sm shadow-sm mb-1">
                        <div class="progress-bar bg-success" role="progressbar" style="width: {{$usersType->pluck('percentage')[1]}}%"></div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="col-lg-6 col-xl-7">
            <div class="card flex-fill w-100">
              <div class="card-header">
                <div class="card-actions float-right">
                  <div class="dropdown show">
                    <a href="#" data-toggle="dropdown" data-display="static">
            <i class="align-middle" data-feather="more-horizontal"></i>
          </a>

                    <div class="dropdown-menu dropdown-menu-right">
                      <a class="dropdown-item" href="#">Action</a>
                      <a class="dropdown-item" href="#">Another action</a>
                      <a class="dropdown-item" href="#">Something else here</a>
                    </div>
                  </div>
                </div>
                <h5 class="card-title mb-0">Real-Time</h5>
              </div>
              <div class="card-body p-2">
                <div id="world_map" style="height:304px;"></div>
              </div>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-12 col-lg-4 d-flex">
            <div class="card flex-fill w-100">
              <div class="card-header">
                <div class="card-actions float-right">
                  <div class="dropdown show">
                    <a href="#" data-toggle="dropdown" data-display="static">
            <i class="align-middle" data-feather="more-horizontal"></i>
          </a>

                    <div class="dropdown-menu dropdown-menu-right">
                      <a class="dropdown-item" href="#">Action</a>
                      <a class="dropdown-item" href="#">Another action</a>
                      <a class="dropdown-item" href="#">Something else here</a>
                    </div>
                  </div>
                </div>
                <h5 class="card-title mb-0">Languages</h5>
              </div>
              <table class="table table-striped my-0">
                <thead>
                  <tr>
                    <th>Language</th>
                    <th class="text-right">Users</th>
                    <th class="d-none d-xl-table-cell w-75">% Users</th>
                  </tr>
                </thead>
                <tbody>
                @foreach ($userLanguages as $userLanguage)
                  <tr>
                    <td>{{$userLanguage['language']}}</td>
                    <td class="text-right">{{$userLanguage['users']}}</td>
                    <td class="d-none d-xl-table-cell">
                      <div class="progress" style="margin-bottom: 0px;">
                        <div class="progress-bar bg-info" role="progressbar" style="width: {{round($userLanguage['percentage'],2)}}%;" aria-valuenow="43" aria-valuemin="0" aria-valuemax="100">{{round($userLanguage['percentage'],2)}}%</div>
                      </div>
                    </td>
                  </tr>
                @endforeach

                </tbody>
              </table>
            </div>
          </div>

          <div class="col-12 col-lg-4 d-flex">
            <div class="card flex-fill w-100">
              <div class="card-header">
                <div class="card-actions float-right">
                  <div class="dropdown show">
                    <a href="#" data-toggle="dropdown" data-display="static">
            <i class="align-middle" data-feather="more-horizontal"></i>
          </a>

                    <div class="dropdown-menu dropdown-menu-right">
                      <a class="dropdown-item" href="#">Action</a>
                      <a class="dropdown-item" href="#">Another action</a>
                      <a class="dropdown-item" href="#">Something else here</a>
                    </div>
                  </div>
                </div>
                <h5 class="card-title mb-0">Visitors / Pages Views</h5>
              </div>
              <div class="card-body d-flex w-100">
                <div class="align-self-center chart">
                  <canvas id="chartjs-dashboard-bar-devices"></canvas>
                </div>
              </div>
            </div>
          </div>

          <div class="col-12 col-lg-4 d-flex">
            <div class="card flex-fill">
              <div class="card-header">
                <div class="card-actions float-right">
                  <div class="dropdown show">
                    <a href="#" data-toggle="dropdown" data-display="static">
            <i class="align-middle" data-feather="more-horizontal"></i>
          </a>

                    <div class="dropdown-menu dropdown-menu-right">
                      <a class="dropdown-item" href="#">Action</a>
                      <a class="dropdown-item" href="#">Another action</a>
                      <a class="dropdown-item" href="#">Something else here</a>
                    </div>
                  </div>
                </div>
                <h5 class="card-title mb-0">Browser</h5>
              </div>
              <div class="card-body">
                <div class="chart">
                  <canvas id="chartjs-dashboard-radar"></canvas>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-12 col-lg-5 col-xl-4 d-flex">
            <div class="card flex-fill w-100">
              <div class="card-header">
                <div class="card-actions float-right">
                  <div class="dropdown show">
                    <a href="#" data-toggle="dropdown" data-display="static">
            <i class="align-middle" data-feather="more-horizontal"></i>
          </a>

                    <div class="dropdown-menu dropdown-menu-right">
                      <a class="dropdown-item" href="#">Action</a>
                      <a class="dropdown-item" href="#">Another action</a>
                      <a class="dropdown-item" href="#">Something else here</a>
                    </div>
                  </div>
                </div>
                <h5 class="card-title mb-0">Mobile / Desktop</h5>
              </div>
              <div class="card-body d-flex">
                <div class="align-self-center w-100">
                  <div class="py-3">
                    <div class="chart chart-xs">
                      <canvas id="chartjs-dashboard-pie"></canvas>
                    </div>
                  </div>

                  <table class="table mb-0">
                    <thead>
                      <tr>
                        <th>Device</th>
                        <th class="text-right">Users</th>
                        <th class="text-right">Users %</th>
                      </tr>
                    </thead>
                    <tbody>

                              @foreach ($userDeviceCategory as $userDevice)
                                <tr>
                                  <td><i class="fas fa-square-full text-primary"></i> {{$userDevice['devices']}}</td>
                                  <td class="text-right">{{$userDevice['users']}}</td>
                                  <td class="text-right text-success">{{round($userDevice['percentage'],2)}}%</td>
                                </tr>
                              @endforeach

                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
          <div class="col-12 col-lg-7 col-xl-8 d-flex">
            <div class="card flex-fill">
              <div class="card-header">
                <div class="card-actions float-right">
                  <div class="dropdown show">
                    <a href="#" data-toggle="dropdown" data-display="static">
            <i class="align-middle" data-feather="more-horizontal"></i>
          </a>

                    <div class="dropdown-menu dropdown-menu-right">
                      <a class="dropdown-item" href="#">Action</a>
                      <a class="dropdown-item" href="#">Another action</a>
                      <a class="dropdown-item" href="#">Something else here</a>
                    </div>
                  </div>
                </div>
                <h5 class="card-title mb-0">Traffic</h5>
              </div>
              <table id="datatables-dashboard-traffic" class="table table-striped my-0">
                <thead>
                  <tr>
                    <th>Source</th>
                    <th class="text-right">Users</th>
                    <th class="d-none d-xl-table-cell text-right">Sessions</th>
                    <th class="d-none d-xl-table-cell text-right">Bounce Rate</th>
                    <th class="d-none d-xl-table-cell text-right">Avg. Session Duration</th>
                  </tr>
                </thead>
                <tbody>

                    @foreach ($traffics as $traffic)
                      <tr>
                        <td>{{$traffic['referrer']}}</td>
                        <td class="text-right">{{$traffic['users']}}</td>
                        <td class="d-none d-xl-table-cell text-right">{{$traffic['sessions']}}</td>
                        <td class="d-none d-xl-table-cell text-right @if ($traffic['bounceRate']) > 50 )
                            text-danger
                          @else
                            text-success
                          @endif">{{round($traffic['bounceRate'],2)}}%</td>
                        <td class="d-none d-xl-table-cell text-right">{{gmdate("H:i:s", $traffic['avgSessionDuration'])}} </td>
                      </tr>
                    @endforeach

                </tbody>
              </table>
            </div>
          </div>
        </div>

      </div>
    </main>

    <script type="text/javascript">

  //   function getActiveVisitors() {
  //           $.get( "{{route('getActiveUser')}}", function( data ) {
  //             console.log(data);
  //             $('#active-visitor').html(data);
  //
  //         });
  //   }
  //
  // $(document).ready(function () {
  //   setInterval(getActiveVisitors,1000);
  // })








    </script>

    <script>
      $(function() {
        // Bar chart
        new Chart(document.getElementById("chartjs-dashboard-bar-devices"), {
          type: "bar",
          data: {
            labels: {!! json_encode($dates->map(function($dates) { return $dates->format('d/m'); })) !!},
            datasets: [{
              label: "Visitors",
              backgroundColor: window.theme.primary,
              borderColor: window.theme.primary,
              hoverBackgroundColor: window.theme.primary,
              hoverBorderColor: window.theme.primary,
              data: {!!json_encode($visitors)!!}
            }, {
              label: "Pages Views",
              backgroundColor: "#E8EAED",
              borderColor: "#E8EAED",
              hoverBackgroundColor: "#E8EAED",
              hoverBorderColor: "#E8EAED",
              data: {!!json_encode($pageViews)!!}
            }]
          },
          options: {
            maintainAspectRatio: false,
            legend: {
              display: false
            },
            scales: {
              yAxes: [{
                gridLines: {
                  display: false
                },
                stacked: false,
                ticks: {
                  stepSize: 20
                }
              }],
              xAxes: [{
                barPercentage: .75,
                categoryPercentage: .5,
                stacked: false,
                gridLines: {
                  color: "transparent"
                }
              }]
            }
          }
        });
      });
    </script>
    <script>
      $(function() {
        $("#world_map").vectorMap({
          map: "world_mill",
          normalizeFunction: "polynomial",
          hoverOpacity: .7,
          hoverColor: false,
          regionStyle: {
            initial: {
              fill: "#e3eaef"
            }
          },
          markerStyle: {
            initial: {
              "r": 9,
              "fill": window.theme.primary,
              "fill-opacity": .95,
              "stroke": "#fff",
              "stroke-width": 7,
              "stroke-opacity": .4
            },
            hover: {
              "stroke": "#fff",
              "fill-opacity": 1,
              "stroke-width": 1.5
            }
          },
          backgroundColor: "transparent",
          zoomOnScroll: false,
          markers: [
    				@foreach ($countries as $country)
    				{


    						latLng: [{{$country['latitude']}},{{$country['longitude']}}],
    						name: "{{$country['country']}}"
    					},
    				@endforeach
          ]
        });
        setTimeout(function() {
          $(window).trigger('resize');
        }, 250)
      });
    </script>
    <script>
      $(function() {
        // Pie chart
        new Chart(document.getElementById("chartjs-dashboard-pie"), {
          type: "pie",
          data: {
            labels: ["Desktop", "Mobile"],
            datasets: [{
              data: {!! json_encode($userDeviceCategory->pluck('users')) !!},
              backgroundColor: [
                window.theme.primary,
                window.theme.warning,
                window.theme.danger,
                "#E8EAED"
              ],
              borderColor: "transparent"
            }]
          },
          options: {
            responsive: !window.MSInputMethodContext,
            maintainAspectRatio: false,
            legend: {
              display: false
            }
          }
        });
      });
    </script>
    <script>
      $(function() {
        // Radar chart
        new Chart(document.getElementById("chartjs-dashboard-radar"), {
          type: "radar",
          data: {
            labels: {!! json_encode($browsers->pluck('browser')) !!},
            datasets: [{
              label: "Interests",
              backgroundColor: "rgba(0, 123, 255, 0.2)",
              borderColor: "#2979ff",
              pointBackgroundColor: "#2979ff",
              pointBorderColor: "#fff",
              pointHoverBackgroundColor: "#fff",
              pointHoverBorderColor: "#2979ff",
              data:  {!! json_encode($browsers->pluck('sessions')) !!}
            }]
          },
          options: {
            maintainAspectRatio: false,
            legend: {
              display: false
            }
          }
        });
      });
    </script>

@endsection
